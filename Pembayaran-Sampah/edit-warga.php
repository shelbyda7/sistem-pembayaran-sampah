<?php 
  session_start();
  if (!isset($_SESSION['login'])) {
    header('location:new_login.php'); 
  }

  include 'koneksi.php';
  $id = $_GET['id'];
  $tampil = mysqli_query($konek,"SELECT * FROM warga WHERE idwarga = $id");
  while ($data = mysqli_fetch_array($tampil)) {
    $nik = $data['nik'];
    $nama = $data['namawarga'];
    $alamat = $data['alamat'];
    $notelp = $data['notelp'];

  }
  
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Edit Warga</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/input-warga.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
   <div class="container">
    <div class="row">
      <div class="main col-md-6 offset-3">
        <h2 class="text-center mt-4">Edit Data Warga</h2>
        <form method="POST">
          <div class="form-group row">
            <div class="col-md-4 offset-1 mt-1">
              <label>NIK </label>
            </div>
            <div class="col-md-4">
              <input type="text" name="nik" class="form-control rounded-pill" value="<?php echo $nik ?>" readonly>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 offset-1 mt-1">
              <label>Nama Warga</label>
            </div>
            <div class="col-md-5">
              <input type="text" name="namawarga" class="form-control rounded-pill" value="<?php echo $nama ?>">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 offset-1 mt-1">
              <label>Alamat</label>
            </div>
            <div class="col-md-5">
              <input type="text" name="alamat" class="form-control rounded-pill" value="<?php echo $alamat ?>">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 offset-1 mt-1">
              <label>No Telp</label>
            </div>
            <div class="col-md-5">
              <input type="text" name="notelp" class="form-control rounded-pill" value="<?php echo $notelp ?>">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 offset-1 mt-1">
              <label>Biaya Sampah</label>
            </div>
            <div class="col-md-3">
              <input type="text" name="biaya" class="form-control rounded-pill" value="24000" readonly>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 offset-1 mt-1">
              <label>Tahun Pembayaran</label>
            </div>
            <div class="col-md-5">
               <input type="text" name="tahunpembayaran" class="form-control rounded-pill" value="2021" readonly>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 offset-1 mt-1">
              <label>Jatuh Tempo Awal</label>
            </div>
            <div class="col-md-5">
              <input type="text" name="jatuhtempo" class="form-control rounded-pill" value="2021-01-10" readonly>
            </div>
          </div>
          <div class="clearfixs">
            <div class="float-right">
              <button class="btn btn-primary shadow" type="submit" name="simpan">Simpan</button>
            </div>
            <div class="float-left">
              <a href="data-warga.php" id="tambah" class="btn btn-primary shadow">Kembali</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</body>
</html>

<!-- simpan data -->
<?php
  if($_SERVER['REQUEST_METHOD']=='POST'){

  //variabel untuk menampung inputan dari form
  $nik   = $_POST['nik'];
  $nama   = $_POST['namawarga'];
  $alamat  = $_POST['alamat'];
  $notelp  = $_POST['notelp'];
  $biaya  = $_POST['biaya'];

  if($nik=='' || $nama =='' || $alamat==''){
    echo "Form Belum lengkap....";
  }else{
    $update = mysqli_query($konek, "UPDATE warga SET namawarga='$nama',alamat='$alamat',notelp='$notelp',biaya='$biaya'WHERE nik='$nik'");

    if(!$update){
      echo "Update data gagal...";

    }else{
      header('location:data-warga.php');
    }
  }
}
?>
