<?php 
  session_start();
	if (!isset($_SESSION['login'])) {
		header('location:index.php');	
	}
  include 'koneksi.php';
  if(isset($_GET['idwarga'])){
  $id = $_GET['idwarga'];
  $tampilWarga = mysqli_query($konek,"SELECT * FROM warga WHERE idwarga = $id");

  while ($data = mysqli_fetch_array($tampilWarga)) {
    $id = $data['idwarga'];
    $nik = $data['nik'];
    $nama = $data['namawarga'];
    $alamat = $data['alamat'];
    $notelp = $data['notelp'];
    $tahun = $data['tahunpembayaran'];
    $biaya = $data['biaya'];
  }
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/input-siswa.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="main col-md-6 offset-3">
        <h2 class="text-center mt-4">Update Siswa</h2>
        <form method="POST">
          <input type="hidden" name="idwarga" value="<?php echo $id ?>">
          <div class="form-group row">
            <div class="col-md-4 offset-1 mt-1">
              <label>NIS </label>
            </div>
            <div class="col-md-4">
              <input type="text" name="nik" class="form-control rounded-pill" value="<?php echo $nik ?>" readonly>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 offset-1 mt-1">
              <label>Nama Siswa</label>
            </div>
            <div class="col-md-5">
              <input type="text" name="namawarga" class="form-control rounded-pill" value="<?php echo $nama ?>">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 offset-1 mt-1">
              <label>Biaya Pendidikan</label>
            </div>
            <div class="col-md-3">
              <input type="text" name="biaya" class="form-control rounded-pill" value="<?php echo $biaya ?>" readonly>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 offset-1 mt-1">
              <label>Tahun Ajaran</label>
            </div>
            <div class="col-md-4">
              <input type="text" name="tahunpembayaran" class="form-control rounded-pill" value="<?php echo $tahun ?>" readonly>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 offset-1 mt-1">
              <label>Jatuh Tempo Awal</label>
            </div>
            <div class="col-md-3">
              <input type="text" name="jatuhtempo" class="form-control rounded-pill" value="2019-07-10" readonly>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 text-center mt-2  ">
              <button class="submit" type="submit" name="simpan">Simpan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</body>
</html>


<!-- simpan data -->
<?php
  if($_SERVER['REQUEST_METHOD']=='POST'){

  //variabel untuk menampung inputan dari form
    $id = $_POST['idwarga'];
    $nik = $_POST['nik'];
    $nama = $_POST['namawarga'];
    $alamat = $_POST['alamat'];
    $notelp = $_POST['notelp'];
    $tahun = $_POST['tahunpembayaran'];
    $biaya = $_POST['biaya'];

  if($nik=='' || $nama =='' || $alamat==''){
    echo "Form Belum lengkap....";
  }else{
    $update = mysqli_query($konek, "UPDATE warga SET nik='$nik',namawarga='$nama',alamat='$alamat',tahunpembayaran='$tahun',biaya='$biaya'WHERE idwarga='$id'");

    if(!$update){
      echo "Update data gagal...";

    }else{
      header('location:data-warga.php');
    }
  }
}
?>
