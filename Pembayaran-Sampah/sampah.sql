-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 30 Sep 2021 pada 11.33
-- Versi server: 10.4.20-MariaDB
-- Versi PHP: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sampah`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `biaya`
--

CREATE TABLE `biaya` (
  `idbiaya` int(100) NOT NULL,
  `nik` int(10) DEFAULT NULL,
  `jatuhtempo` date DEFAULT NULL,
  `bulan` varchar(20) DEFAULT NULL,
  `nobayar` varchar(10) DEFAULT NULL,
  `tglbayar` date DEFAULT NULL,
  `jumlah` int(20) DEFAULT NULL,
  `ket` varchar(20) DEFAULT NULL,
  `idadmin` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `biaya`
--

INSERT INTO `biaya` (`idbiaya`, `nik`, `jatuhtempo`, `bulan`, `nobayar`, `tglbayar`, `jumlah`, `ket`, `idadmin`) VALUES
(1, 1234, '2021-01-10', 'Januari 2021', '2108050001', '2021-08-05', 20000, 'LUNAS', 1),
(2, 1234, '2021-02-10', 'Februari 2021', '2109230001', '2021-09-23', 20000, 'LUNAS', 1),
(3, 1234, '2021-03-10', 'Maret 2021', '2109230002', '2021-09-23', 20000, 'LUNAS', 1),
(4, 1234, '2021-04-10', 'April 2021', '2109270001', '2021-09-27', 20000, 'LUNAS', 0),
(5, 1234, '2021-05-10', 'Mei 2021', NULL, NULL, 20000, NULL, NULL),
(6, 1234, '2021-06-10', 'Juni 2021', NULL, NULL, 20000, NULL, NULL),
(7, 1234, '2021-07-10', 'Juli 2021', NULL, NULL, 20000, NULL, NULL),
(8, 1234, '2021-08-10', 'Agustus 2021', NULL, NULL, 20000, NULL, NULL),
(9, 1234, '2021-09-10', 'September 2021', NULL, NULL, 20000, NULL, NULL),
(10, 1234, '2021-10-10', 'Oktober 2021', NULL, NULL, 20000, NULL, NULL),
(11, 1234, '2021-11-10', 'November 2021', NULL, NULL, 20000, NULL, NULL),
(12, 1234, '2021-12-10', 'Desember 2021', NULL, NULL, 20000, NULL, NULL),
(13, 1235, '2021-01-10', 'Januari 2021', '2109200001', '2021-09-20', 24, 'LUNAS', 1),
(14, 1235, '2021-02-10', 'Februari 2021', '2109200002', '2021-09-20', 24, 'LUNAS', 1),
(15, 1235, '2021-03-10', 'Maret 2021', '2109200003', '2021-09-20', 24, 'LUNAS', 1),
(16, 1235, '2021-04-10', 'April 2021', NULL, NULL, 24, NULL, NULL),
(17, 1235, '2021-05-10', 'Mei 2021', NULL, NULL, 24, NULL, NULL),
(18, 1235, '2021-06-10', 'Juni 2021', NULL, NULL, 24, NULL, NULL),
(19, 1235, '2021-07-10', 'Juli 2021', NULL, NULL, 24, NULL, NULL),
(20, 1235, '2021-08-10', 'Agustus 2021', NULL, NULL, 24, NULL, NULL),
(21, 1235, '2021-09-10', 'September 2021', NULL, NULL, 24, NULL, NULL),
(22, 1235, '2021-10-10', 'Oktober 2021', NULL, NULL, 24, NULL, NULL),
(23, 1235, '2021-11-10', 'November 2021', NULL, NULL, 24, NULL, NULL),
(24, 1235, '2021-12-10', 'Desember 2021', NULL, NULL, 24, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `idadmin` int(5) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `namalengkap` varchar(40) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`idadmin`, `username`, `password`, `namalengkap`, `level`) VALUES
(1, 'admin', 'admin', 'rahma delia', 'admin'),
(2, 'adel', 'adel', 'adeliarahm', 'warga');

-- --------------------------------------------------------

--
-- Struktur dari tabel `warga`
--

CREATE TABLE `warga` (
  `idwarga` int(10) NOT NULL,
  `nik` int(20) NOT NULL,
  `namawarga` varchar(40) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `notelp` varchar(15) NOT NULL,
  `tahunpembayaran` varchar(11) NOT NULL,
  `biaya` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `warga`
--

INSERT INTO `warga` (`idwarga`, `nik`, `namawarga`, `alamat`, `notelp`, `tahunpembayaran`, `biaya`) VALUES
(1, 1234, 'delia', 'blok bt:22', '08912367819', '2021', 24000),
(2, 1235, 'Sri', 'blok bt:17', '08962138190', '2021', 24);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `biaya`
--
ALTER TABLE `biaya`
  ADD PRIMARY KEY (`idbiaya`),
  ADD KEY `nik` (`nik`),
  ADD KEY `idadmin` (`idadmin`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idadmin`);

--
-- Indeks untuk tabel `warga`
--
ALTER TABLE `warga`
  ADD PRIMARY KEY (`idwarga`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `biaya`
--
ALTER TABLE `biaya`
  MODIFY `idbiaya` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `warga`
--
ALTER TABLE `warga`
  MODIFY `idwarga` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
