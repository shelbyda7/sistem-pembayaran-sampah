<?php include '../koneksi.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Transaksi</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/transaksi.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="main col-md-8">
        <form method="GET" action="laporan-pembayaran.php" target="_blank">
          <div class="form-group row mt-2">
            <input type="text" name="nik" placeholder="Masukan NIK Warga" class="form-control-plaintext col-md-7 offset-1 ">
            <button class="btn submit col-md-3" name="cari" type="submit">Cari</button>
          </div>
        </form>
      </div>
    </div>
    <?php
      if(isset($_GET['nik']) && $_GET['nik']!=''){
        $sqlWarga = mysqli_query($konek, "SELECT * FROM warga WHERE nik='$_GET[nik]'");
        $ds=mysqli_fetch_array($sqlWarga);
        $nik = $ds['nik'];
      ?>

      
        </div>
      </div>

      <?php
    }
  ?>
  </div>
  <div class="container">
    <div class="float-right">
      <a href="signout.php" class="btn mt-4"><i class="fa fa-sign-out"> Sign Out</i></a>
    </div>
  
    <p class="text-center mt-1">PERUMAHAN LEMBAH BUKIT CALINCING <br> RT04/RW08</p>
  </div>
</body>
</html>