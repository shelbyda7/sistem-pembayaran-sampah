<?php include '../koneksi.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Transaksi</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/transaksi.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<?php
      if(isset($_GET['nik']) && $_GET['nik']!=''){
        $sqlWarga = mysqli_query($konek, "SELECT * FROM warga WHERE nik='$_GET[nik]'");
        $ds=mysqli_fetch_array($sqlWarga);
        $nik = $ds['nik'];
      ?>
	<h2 class="text-center mt-4 mb-3">Tagihan Pembayaran Sampah <?php echo $ds['namawarga']; ?></h2>
      <div class="row">
        <div class="col-sm-8 offset-2">
          <table id="transaksi" class="table table-bordered table-sm table-hover text-center shadow">
            <thead id="thead">
              <tr>
                <th>No</th>
                <th>Bulan</th>
                <th>Tgl. Bayar</th>
                <th>Jumlah</th>
                <th>Keterangan</th>
              </tr>
            </thead>
            <tbody id="tbody">
            <?php
            $sql = mysqli_query($konek, "SELECT * FROM biaya WHERE nik='$ds[nik]' ORDER BY jatuhtempo ASC");
            $no=1;
            while($d=mysqli_fetch_array($sql)){
              echo "<tr>
                <td>$no</td>
                <td>$d[bulan]</td>
                <td>$d[tglbayar]</td>
                <td>$d[jumlah]</td>
                <td>$d[ket]</td>
              </tr>";
              $no++;
            }
            ?>
            </tbody>
          </table>
          <?php
    }
  ?>
          <table width="80%" align="center">
    <tr>
      <td></td>
      <td width="200px">
        <p>Bogor, <?php echo date('Y-m-d') ?><br>Operator</p>
        <br><br>
        <p>_________________</p>
        <a href="#" class="no-print" onclick="window.print();">Cetak Laporan</a>
      </td>
    </tr>
  </table>