<?php

class Auth extends CI_Controller
{
    public function login()
    {
        check_login();
        $this->load->view('login');
    }

    public function process()
    {
        $post = $this->input->post(null, TRUE);
        if (isset($post['login'])) {
            $this->load->model('Muser');
            $query = $this->Muser->login($post);
            if ($query->num_rows() > 0) {
                $row = $query->row();
                $params = array(
                    'id' => $row->id,
                    'email' => $row->email,
                    'username' => $row->username,
                    'role' => $row->role
                );
                $this->session->set_userdata($params);
                if ($row->role == 2) {
                    echo "<script>
                        alert('Selamat, login berhasil');
                        window.location='" . site_url('tagihan') . "';    
                    </script>";
                } else {
                    echo "<script>
                        alert('Selamat, login berhasil');
                        window.location='" . site_url('tagihan') . "';    
                    </script>";
                }
            } else {
                echo "<script>
                    alert('Login gagal, username atau password salah');
                    window.location='" . site_url('auth/login') . "';    
                </script>";
            }
        } else {
            echo "<script>window.location='" . site_url('auth/login') . "';</script>";
        }
    }

    public function logout()
    {
        $params = array('id', 'role');
        $this->session->unset_userdata($params);
        redirect('auth/login');
    }
}
