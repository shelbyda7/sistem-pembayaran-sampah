<?php

class Dashboard extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        check_not_login();
        $this->load->model('Muser');
    }

    public function index()
    {
        check_not_login();
        $this->template->load('template', 'dashboard/dashboard');
    }
}
