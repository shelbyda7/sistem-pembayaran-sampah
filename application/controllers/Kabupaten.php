<?php

class Kabupaten extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mkabupaten');
        $this->load->model('Mprovinsi');
        $this->load->model('Mnegara');
    }

    public function index()
    {
        check_not_login();
        $data['result'] = $this->Mkabupaten->show_data()->result();
        $data['provinsi'] = $this->Mprovinsi->get()->result();
        $data['negara'] = $this->Mnegara->get()->result();
        $this->template->load('template', 'kabupaten/list', $data);
    }

    public function add()
    {
        $data = array (
            'nama_kabupaten'=> $this->input->post('nama_kabupaten'),
            'id_provinsi'  => $this->input->post('id_provinsi')
          );
          $this->Mkabupaten->add($data);
          echo "<script>window.location='" . site_url('Kabupaten') . "';</script>";
    }

    public function edit()
    {
      $data = array (
        'id'                => $this->input->post('id_kabupaten'),
        'nama_kabupaten'     => $this->input->post('nama_kabupaten'),
        'id_provinsi'       => $this->input->post('id_provinsi')
      );
      $this->Mkabupaten->edit($data);
      echo "<script>window.location='" . site_url('Kabupaten') . "';</script>";
    }

    public function check_kabupaten()
    {
      $id=$this->input->post('id');
      $data=$this->Mkabupaten->check_kabupaten($id);
      echo json_encode($data);
    }

    public function del($id)
    {
        $this->Mkabupaten->del($id);
        echo "<script>window.location='" . site_url('Kabupaten') . "';</script>";
    }
}
