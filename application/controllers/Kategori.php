<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	function __construct() {
      parent::__construct();
      $this->load->model('Mkategori');
    }

	public function index()
	{
    check_not_login();
    $data['kategori'] = $this->Mkategori->get()->result();
		$this->template->load('template','kategori/list', $data);
  }

  public function add()
    {
      $biaya = (float) preg_replace("/[^0-9]/", "", $_POST['biaya']);
      $data = array (
        'nama'    => $this->input->post('nama'),
        'biaya'   => $biaya
      );
      $this->Mkategori->add($data);
      echo "<script>window.location='" . site_url('Kategori') . "';</script>";
    }

    public function edit()
    {
      $biaya = (float) preg_replace("/[^0-9]/", "", $_POST['biaya']);
      $data = array (
        'id'      => $this->input->post('id'),
        'nama'    => $this->input->post('nama'),
        'biaya'   => $biaya
      );
      $this->Mkategori->edit($data);
      echo "<script>window.location='" . site_url('Kategori') . "';</script>";
    }

    public function del($id)
    {
        $this->Mkategori->del($id);
        if ($this->db->affected_rows() > 0) {
            $this->session->set_flashdata('success', 'Data Berhasil Dihapus');
        }
        echo "<script>window.location='" . site_url('kategori') . "';</script>";
    }
}
