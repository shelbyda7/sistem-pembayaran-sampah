<?php

class Negara extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mnegara');
    }

    public function index()
    {
        check_not_login();
        $data['result'] = $this->Mnegara->get()->result();
        $this->template->load('template', 'negara/list', $data);
    }

    public function add()
    {
        $data = array (
            'nama_negara'    => $this->input->post('nama_negara')
          );
          $this->Mnegara->add($data);
          echo "<script>window.location='" . site_url('Negara') . "';</script>";
    }

    public function edit()
    {
      $data = array (
        'id'             => $this->input->post('id'),
        'nama_negara'    => $this->input->post('nama_negara'),
      );
      $this->Mnegara->edit($data);
      echo "<script>window.location='" . site_url('Negara') . "';</script>";
    }

    public function del($id)
    {
        $this->Mnegara->del($id);
        echo "<script>window.location='" . site_url('Negara') . "';</script>";
    }
}
