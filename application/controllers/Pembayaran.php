<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Pembayaran extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('Mpembayaran');
    }
    
    function update_pembayaran($id_pembayaran = null, $bulan = null, $nama_anggota = null, $tgl_bayar = null, $status_tagihan = null, $status_pembayaran = null, $id_tagihan = null, $created_by = null, $catatan = null, $image = null, $metode = null){
		$update_date	= date("Y-m-d h:i:s");
        if (@$_FILES['image']['name'] != null) {
			$img = $this->Mpembayaran->get($id_tagihan)->row();
            if ($img != null) {
				if ($this->upload->do_upload('image')) {
					$image = $this->upload->data('file_name');
                    $data = array(
						'tgl_bayar'		=> date("Y-m-d h:i:s", strtotime($tgl_bayar)),
                        'image'			=> '/uploads/'.$image,
                        'updated_date'	=> date("Y-m-d h:i:s", strtotime($update_date)),
                        'status'		=> $status_pembayaran,
                        'id_tagihan'	=> $id_tagihan,
                        'created_by'	=> $created_by,
                        'catatan'		=> $catatan,
                        'metode'		=> $metode
                    );
                    
                    $data_update = array(
						'status'		=> $status_tagihan
                    );
					
                    $this->db->where('id', $id_pembayaran);
                    $this->db->update('pembayaran',$data);
                    $this->db->where('id', $id_tagihan);
                    $this->db->update('tagihan',$data_update);
                    echo "<script>alert('Anda Berhasil Menyimpan');</script>";
					if ($this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) {
						echo "<script>window.location='" . site_url('Pembayaran') . "';</script>";
					} else {
						echo "<script>window.location='" . site_url('tagihan/detail/'.$this->session->userdata('id')) . "';</script>";
					}
                }
            }
        } else {
            $image = $this->upload->data('file_name');
            $data = array(
                'tgl_bayar'		=> date("Y-m-d h:i:s", strtotime($tgl_bayar)),
                'updated_date'	=> date("Y-m-d h:i:s", strtotime($update_date)),
                'status'		=> $status_pembayaran,
                'id_tagihan'	=> $id_tagihan,
                'created_by'	=> $created_by,
				'catatan'		=> $catatan,
                'metode'		=> $metode,
            );
            
            $data_update = array(
                'status'		=> $status_tagihan
            );
            $this->db->where('id',$id_pembayaran);
            $this->db->update('pembayaran',$data);
            $this->db->where('id', $id_tagihan);
            $this->db->update('tagihan',$data_update);
            echo "<script>alert('Anda Berhasil Menyimpan');</script>";
			if ($this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) {
            echo "<script>window.location='" . site_url('Pembayaran') . "';</script>";
			} else {
				echo "<script>window.location='" . site_url('tagihan/detail/'.$this->session->userdata('id')) . "';</script>";
			}
        }
    }

	public function index()
	{
		check_not_login();
		if ($this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) {
			$data['result']	= $this->Mpembayaran->show_data()->result();
			$this->template->load('template','pembayaran/list', $data);
		} else {
			$data['result']	= $this->Mpembayaran->show_data_user($this->session->userdata('id'))->result();
			$this->template->load('template','pembayaran/list', $data);
		}
	}

	public function check_no_tagihan()
	{
		if (isset($_POST['no_tagihan'])){
			$no_tagihan = $this->Mpembayaran->check_no_tagihan($_POST['no_tagihan']);
			if ($no_tagihan->num_rows() > 0){
				foreach ($no_tagihan->result() as $value) {
					$data = array(
						'id'				=> $value->id_tagihan,
						'nama'				=> $value->nama,
						'no_tagihan'		=> $value->no_tagihan,
						'bulan'				=> $value->bulan,
						'biaya'				=> "Rp. " . number_format($value->biaya, 0, ".", "."),
						'status_tagihan'	=> $value->status_tagihan
					);
					echo json_encode($data);
				}
			} else {
				$result['res'] = "N/A";
				echo json_encode($result);
			}
		}
	}

	public function proccess()
	{
		$id_pembayaran	= $this->input->post('id_pembayaran');
		$bulan			= $this->input->post('bulan');
		$nama_anggota	= $this->input->post('nama');
		$tgl_bayar		= $this->input->post('tgl_bayar');
		$created_date	= date("Y-m-d h:i:s");
		$update_date	= date("Y-m-d h:i:s");
		$status_tagihan	= $this->input->post('status_tagihan');
		$status_pembayaran = $this->input->post('status_pembayaran');
		$id_tagihan		= $this->input->post('id_tagihan');
		$created_by		= $this->session->id;
		$catatan		= $this->input->post('catatan');
		$action			= $this->input->post('submit');
		$image			= $this->input->post('image');
		$metode			= $this->input->post('metode');
		
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg|pdf|csv|rar|zip|doc|docx|xls|xlsx|ppt|pptx|odt';
		$config['max_size']             = 10240;
		$config['file_name']			= 'Bukti-Pembayaran'.'-'.$nama_anggota.'-'.$bulan;
		$this->load->library('upload', $config);
		
		if($action == "tambah"){
			if (@$_FILES['image']['name'] != null) {
				$img = $this->Mpembayaran->get($id_tagihan)->row();
				$status_tagihan = "Menunggu Validasi";
				if ($img == null) {
						if ($this->upload->do_upload('image')) {
						$image = $this->upload->data('file_name');
						$data = array(
							'tgl_bayar'		=> date("Y-m-d h:i:s", strtotime($tgl_bayar)),
							'image'			=> '/uploads/'.$image,
							'created_date'	=> date("Y-m-d h:i:s", strtotime($created_date)),
							'updated_date'	=> date("Y-m-d h:i:s", strtotime($update_date)),
							'status'		=> $status_pembayaran,
							'id_tagihan'	=> $id_tagihan,
							'created_by'	=> $created_by,
							'catatan'		=> $catatan,
							'metode'		=> $metode
						);

						$data_update = array(
							'status'		=> $status_tagihan
						);
						
						$this->db->insert('pembayaran',$data);
						$this->db->where('id', $id_tagihan);
						$this->db->update('tagihan',$data_update);
						echo "<script>alert('Anda Berhasil Menyimpan');</script>";
						if ($this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) {
							echo "<script>window.location='" . site_url('Pembayaran') . "';</script>";
						} else {
							echo "<script>window.location='" . site_url('tagihan/detail/'.$this->session->userdata('id')) . "';</script>";
						}
					}
				} else {
					echo "<script>alert('Gagal Disimpan! Log Pembyaran Sudah Pernah Dibuat, Check No. Tagihan Anda');</script>";
					if ($this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) {
						echo "<script>window.location='" . site_url('Pembayaran') . "';</script>";
					} else {
						echo "<script>window.location='" . site_url('tagihan/detail/'.$this->session->userdata('id')) . "';</script>";
					}
				}
			} else {
				show_error($this->upload->display_errors());
			}
		} else {
			if($status_pembayaran == "Dalam Pengecekkan") {
				$status_tagihan = "Menunggu Validasi";
                $this->update_pembayaran($id_pembayaran, $bulan, $nama_anggota, $tgl_bayar, $status_tagihan, $status_pembayaran, $id_tagihan, $created_by, $catatan, $image, $metode);
			} else if($status_pembayaran == "Di Terima") {
				$status_tagihan = "Lunas";
                $this->update_pembayaran($id_pembayaran, $bulan, $nama_anggota, $tgl_bayar, $status_tagihan, $status_pembayaran, $id_tagihan, $created_by, $catatan, $image, $metode);
			} else if($status_pembayaran == "Di Tolak") {
				$status_tagihan = "Belum Lunas";
                $this->update_pembayaran($id_pembayaran, $bulan, $nama_anggota, $tgl_bayar, $status_tagihan, $status_pembayaran, $id_tagihan, $created_by, $catatan, $image, $metode);
			}
		}
	}

	public function del($id)
    {
        $this->Mpembayaran->del($id);
        echo "<script>window.location='" . site_url('pembayaran') . "';</script>";
    }

	public function check_metode(){
        $value=$this->input->post('value');
        $data=$this->Mpembayaran->metode($value);
        echo json_encode($data);
    }
}
