<?php

class Pengguna extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        check_not_login();
        $this->load->model('Muser');
        $this->load->model('Mkabupaten');
        $this->load->model('Mprovinsi');
        $this->load->model('Mnegara');
    }

    public function index()
    {
        $data['staff'] = $this->Muser->admin()->result();
        $data['anggota'] = $this->Muser->anggota()->result();
        $this->template->load('template', 'pengguna/list', $data);
    }

    public function check_kewarganegaraan(){
        $value=$this->input->post('value');
        $data=$this->Mnegara->check_negara($value);
        echo json_encode($data);
    }

    public function add()
    {
        $pengguna = new stdClass();
        $pengguna->id = null;
        $pengguna->nik = null;
        $pengguna->nama = null;
        $pengguna->tempat_lahir = null;
        $pengguna->tgl_lahir = null;
        $pengguna->jk = null;
        $pengguna->alamat = null;
        $pengguna->contact = null;
        $pengguna->rt_rw = null;
        $pengguna->negara = null;
        $pengguna->kelurahan = null;
        $pengguna->kecamatan = null;
        $pengguna->agama = null;
        $pengguna->status_kawin = null;
        $pengguna->pekerjaan = null;
        $pengguna->kewarganegaraan = null;
        $pengguna->username = null;
        $pengguna->password = null;
        $pengguna->email = null;
        $pengguna->password_decrypt = null;
        $pengguna->role = null;
        $pengguna->id_kabupaten = null;

        $data = array(
            'page'      => 'tambah',
            'row'       => $pengguna,
            'kabupaten' => $this->Mkabupaten->show_data()->result()
        );
        $this->template->load('template', 'pengguna/addedit', $data);
    }

    public function edit($id)
    {

        $query = $this->Muser->get($id);
        if ($query->num_rows() > 0) {
            $product = $query->row();
            $data = array(
                'page'  => 'edit',
                'kabupaten' => $this->Mkabupaten->get()->result(),
                'row'   => $product
            );
            $this->template->load('template', 'pengguna/addedit', $data);
        }
    }

    public function process()
    {
        
            $post = $this->input->post(null, TRUE);
            if (isset($post['tambah'])) {
                $this->Muser->add($post);
            } elseif (isset($post['edit'])) {
                $this->Muser->edit($post);
            }
            echo "<script>window.location='" . site_url('Pengguna') . "';</script>";
    }

    public function del($id)
    {
        $this->Muser->del($id);
        if ($this->db->affected_rows() > 0) {
            $this->session->set_flashdata('success', 'Data Berhasil Dihapus');
        }
        echo "<script>window.location='" . site_url('Pengguna') . "';</script>";
    }
}
