<?php

class Provinsi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mprovinsi');
        $this->load->model('Mnegara');
    }

    public function index()
    {
        check_not_login();
        $data['result'] = $this->Mprovinsi->show_data()->result();
        $data['negara'] = $this->Mnegara->get()->result();
        $this->template->load('template', 'provinsi/list', $data);
    }

    public function add()
    {
        $data = array (
            'nama_provinsi'=> $this->input->post('nama_provinsi'),
            'id_negara'    => $this->input->post('id_negara')
          );
          $this->Mprovinsi->add($data);
          echo "<script>window.location='" . site_url('Provinsi') . "';</script>";
    }

    public function check_provinsi()
    {
        $id=$this->input->post('id');
        $data=$this->Mprovinsi->check_provinsi($id);
        echo json_encode($data);
    }

    public function edit()
    {
      $data = array (
        'id'                => $this->input->post('id'),
        'nama_provinsi'     => $this->input->post('nama_provinsi'),
        'nama_negara'       => $this->input->post('nama_negara')
      );
      $this->Mprovinsi->edit($data);
      echo "<script>window.location='" . site_url('Provinsi') . "';</script>";
    }

    public function del($id)
    {
        $this->Mprovinsi->del($id);
        echo "<script>window.location='" . site_url('Provinsi') . "';</script>";
    }
}
