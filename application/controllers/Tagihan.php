<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tagihan extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('Mtagihan');
		$this->load->model('Mkategori');
		$this->load->model('Mpembayaran');
    }

	public function index()
	{
		check_not_login();
		if ($this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) {
			$data['anggota'] = $this->Mtagihan->anggota()->result();
			$this->template->load('template','tagihan/list', $data);
		} elseif ($this->session->userdata('role') == 3) { 
			$id_user = $this->session->userdata('id');
			$data['anggota'] = $this->detail($id_user);
		} 
	}

	public function detail($id)
    {
		$data['anggota'] = $this->Mtagihan->get($id)->result();
		$data['tagihan'] = $this->Mtagihan->daftar_tagihan($id)->result();
		$data['kategori'] = $this->Mkategori->get()->result();
		$data['result']	= $this->Mtagihan->pembayaran_tagihan($id)->result();
		$this->template->load('template', 'tagihan/detail', $data);
    }


	public function check_nik()
	{
		if (isset($_POST['nik'])){
			$nik = $this->Mtagihan->check_nik($_POST['nik']);
			if ($nik->num_rows() > 0){
				foreach ($nik->result() as $value) {
					$data = array(
						'id'		=> $value->id,
						'nama'		=> $value->nama,
						'email'		=> $value->email,
						'contact'	=> $value->contact,
					);
					echo json_encode($data);
				}
			} else {
				$result['res'] = "N/A";
				echo json_encode($result);
			}
		}
	}

	public function check_kategori()
	{
		if (isset($_POST['id_kategori'])){
			$kategori = $this->Mtagihan->check_kategori($_POST['id_kategori']);
			if ($kategori->num_rows() > 0){
				foreach ($kategori->result() as $value) {
					$data = array(
						'biaya'		=> $value->biaya
					);
					echo json_encode($data);
				}
			} else {
				$result['res'] = "N/A";
				echo json_encode($result);
			}
		}
	}

	public function add()
	{
		$bulan = date('m');
		$tanggal = date('d');
		$tahun = date('Y');
		$datetime = $tahun . $bulan . $tanggal . '-';

		$input					= new stdClass();
        $input->id_tagihan		= null;
        $input->nik				= null;
		$input->nama			= null;
		$input->email			= null;
		$input->contact			= null;
		$input->no_tagihan		= null;
		$input->bulan			= null;
		$input->id_user			= null;
		$input->due_date		= null;
		$input->biaya			= null;
		$input->catatan			= null;
		$input->status			= null;
		$input->id_kategori 	= null;
		$input->tagihan_kategori= null;
		$input->nama_kategori	= null;

		$data = array(
			'page'		=> 'tambah',
			'row'		=> $input,
			'kategori' 	=> $this->Mkategori->get()->result(),
			'date'		=> $datetime,
			'tagihan'	=> $this->Mtagihan->no_tagihan('tagihan', $datetime)->row(),
		);
		$this->template->load('template','tagihan/add', $data);
	}
	
	public function edit($id_user, $id)
    {
		
		$query = $this->Mtagihan->detail_tagihan($id_user, $id);
		$bulan = date('m');
		$tanggal = date('d');
		$tahun = date('y');
		$datetime = $tanggal . $bulan . $tahun . '-';
		
		if ($query->num_rows() > 0) {
			$input = $query->row();
            $data = array(
				'page'      => 'edit',
                'row'       => $input,
				'kategori'  => $this->Mkategori->get()->result(),
                'date'		=> $datetime,
				'tagihan'	=> $this->Mtagihan->no_tagihan('tagihan', $datetime)->row(),
            );
            $this->template->load('template', 'tagihan/add', $data);
        }
    }

	public function proccess()
    {
        $post = $this->input->post(null, TRUE);
        if (isset($post['tambah'])) {
			$this->Mtagihan->add($post);
			echo "<script>window.location='" . site_url('tagihan') . "';</script>";
        } elseif (isset($post['edit']) && isset($post['id_user'])) {
            $this->Mtagihan->edit($post);
			echo "<script>window.location='" . base_url('tagihan/detail/') . $_POST['id_user'] ."';</script>";
        }
    }

	public function del($id_user, $id)
    {
        $this->Mtagihan->del($id_user, $id);
        echo "<script>window.location='" . site_url('tagihan') . "';</script>";
    }
}
