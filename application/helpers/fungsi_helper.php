<?php

function check_login()
{
    $ci =& get_instance();
    $user_session = $ci->session->userdata('id');
    if ($user_session) {
        redirect('tagihan');
    }
}

function check_not_login()
{
    $ci =& get_instance();
    $user_session = $ci->session->userdata('id');
    if (!$user_session) {
        redirect('auth/login');
    }
}