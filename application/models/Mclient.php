<?php 

class Mclient extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('client');
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query;
    }

    public function add($post)
    {
        $params = [
            'id_pic' =>$post['id_pic'],
            'cif' => $post['cif'],
            'nama' => $post['nama'],
            'group' => $post['group'],
            'bsns_unt' =>$post['bsns_unt'],
            'category' =>$post['category'],
            'comp_id' =>$post['comp_id'],
        ];
        $this->db->insert('client', $params);
    }

    public function edit($post)
    {
        $params = [
            'id_pic' =>$post['id_pic'],
            'cif' =>$post['cif'],
            'nama' =>$post['nama'],
            'group' =>$post['group'],
            'bsns_unt' =>$post['bsns_unt'],
            'category' =>$post['category'],
            'comp_id' =>$post['comp_id'],
        ];
        $this->db->where('id', $post['id']);
        $this->db->update('client', $params);
    }

    public function del($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('client');
    }
}
?>
