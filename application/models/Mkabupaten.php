<?php 

class Mkabupaten extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('kabupaten');
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query;
    }

    public function show_data()
    {
        $sql = "SELECT kabupaten.id as id_kabupaten, kabupaten.nama_kabupaten, kabupaten.id_provinsi, provinsi.id as id, provinsi.nama_provinsi, negara.id as id_negara, negara.nama_negara FROM kabupaten
        INNER JOIN provinsi ON kabupaten.id_provinsi = provinsi.id
        INNER JOIN negara ON provinsi.id_negara = negara.id";
        return $this->db->query($sql);
    }

    public function check_kabupaten($id)
    {
        $hasil=$this->db->query("SELECT kabupaten.id as id_kabupaten, kabupaten.nama_kabupaten, kabupaten.id_provinsi, provinsi.id as id, provinsi.nama_provinsi, negara.id as id_negara, negara.nama_negara FROM kabupaten
        INNER JOIN provinsi ON kabupaten.id_provinsi = provinsi.id
        INNER JOIN negara ON provinsi.id_negara = negara.id
        WHERE kabupaten.id_provinsi = $id ORDER BY kabupaten.nama_kabupaten ASC");
        return $hasil->result();
    }

    public function add($post)
    {
        $status = $this->db->insert('kabupaten', $post);
        return $status;
    }

    public function edit($post)
    {
        $this->db->where('id', $post['id']);
        $status = $this->db->update('kabupaten', $post);
        return $status;
    }

    public function del($id)
    {
        
        $this->db->where('id', $id);
        $this->db->delete('kabupaten');
    }

    public function select($table, $order)
    {
        $sql = "SELECT * FROM $table ORDER BY $order ASC";
        return $this->db->query($sql);
    }
}
?>
