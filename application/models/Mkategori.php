<?php

class Mkategori extends CI_Model {

    public function get($id = null)
    {
        $this->db->from('kategori');
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query;
    }
    
    public function add($post)
    {
        $status = $this->db->insert('kategori', $post);
        return $status;
    }

    public function edit($post)
    {
        $this->db->where('id', $post['id']);
        $status = $this->db->update('kategori', $post);
        return $status;
    }

    public function del($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('kategori');
    }
}