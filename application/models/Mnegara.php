<?php 

class Mnegara extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('negara');
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query;
    }

    public function add($post)
    {
        $status = $this->db->insert('negara', $post);
        return $status;
    }

    public function edit($post)
    {
        $this->db->where('id', $post['id']);
        $status = $this->db->update('negara', $post);
        return $status;
    }

    public function del($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('negara');
    }

    public function check_negara($value)
    {
        if($value == "WNI"){
            $sql = "SELECT * FROM negara WHERE id = 77";
            $query = $this->db->query($sql);
            $result = $query->result_array();
        } else {
            $sql = "SELECT * FROM negara WHERE id != 77";
            $query = $this->db->query($sql);
            $result = $query->result_array();
        }
        return $result;
    }

    public function select($table, $order)
    {
        $sql = "SELECT * FROM $table ORDER BY $order ASC";
        return $this->db->query($sql);
    }
}
?>
