<?php

class Mpembayaran extends CI_Model {

    public function show_data()
    {
        $sql = "SELECT pembayaran.id as id_pembayaran, user.nama, tagihan.id AS id_tagihan, no_tagihan, tagihan.status as status_tagihan, bulan, due_date, biaya,id_user, pembayaran.id AS id_pembayaran, tgl_bayar, image, pembayaran.status, pembayaran.catatan, pembayaran.metode
        FROM user
        JOIN tagihan ON tagihan.id_user = user.id
        JOIN pembayaran ON tagihan.id = pembayaran.id_tagihan
        ORDER BY pembayaran.id DESC";
        return $this->db->query($sql);
    }

    public function show_data_user($id)
    {
        $sql = "SELECT pembayaran.id as id_pembayaran, user.nama, tagihan.id AS id_tagihan, no_tagihan, tagihan.status as status_tagihan, bulan, due_date, biaya,id_user, pembayaran.id AS id_pembayaran, tgl_bayar, image, pembayaran.status, pembayaran.catatan, pembayaran.metode
        FROM user
        JOIN tagihan ON tagihan.id_user = user.id
        JOIN pembayaran ON tagihan.id = pembayaran.id_tagihan
        WHERE user.id = $id
        ORDER BY pembayaran.id DESC";
        return $this->db->query($sql);
    }

    public function check_no_tagihan($no_tagihan)
    {
        $sql = "SELECT user.id as id_user, user.nama, tagihan.id AS id_tagihan, tagihan.no_tagihan, tagihan.bulan, tagihan.biaya, tagihan.status as status_tagihan
        FROM user 
        JOIN tagihan ON tagihan.id_user = user.id 
        WHERE tagihan.no_tagihan = '$no_tagihan'";
        return $this->db->query($sql);
    }

    public function add()
    {
        $sql = "SELECT * FROM log WHERE id=$id";
        return $this->db->query($sql);
    }

    public function get($id)
    {
        $sql = "SELECT * FROM pembayaran
        INNER JOIN tagihan ON tagihan.id = pembayaran.id_tagihan
        WHERE tagihan.id =$id";
        return $this->db->query($sql);
    }

    public function del($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('pembayaran');
    }

    public function metode($value){
		if($value == "Transfer"){
            $result = array(
                'bank' => "Kirim Ke-Bank BCA 123456780 Atas Nama Delia Rahma dan Upload Bukti Pembayaran Anda"
            );
        } else {
            $result = array(
                'bank' => "Upload Bukti Pembayaran Anda"
            );
        }
        return $result;
	}

}