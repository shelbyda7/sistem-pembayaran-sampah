<?php 

class Mproduct extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('product');
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query;
    }

    public function add($post)
    {
        $params = [
            'product' =>$post['product']
        ];
        $this->db->insert('product', $params);
    }

    public function edit($post)
    {
        $params = [
            'product' => $post['product']
        ];
        $this->db->where('id', $post['id']);
        $this->db->update('product', $params);
    }

    public function del($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('product');
    }
}
?>
