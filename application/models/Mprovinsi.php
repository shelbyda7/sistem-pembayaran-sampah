<?php 

class Mprovinsi extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('provinsi');
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query;
    }

    public function show_data()
    {
        $sql = "SELECT provinsi.id as id_provinsi, provinsi.nama_provinsi, negara.id as id_negara, negara.nama_negara FROM provinsi
        INNER JOIN negara ON provinsi.id_negara = negara.id";
        return $this->db->query($sql);
    }

    public function check_provinsi($id)
    {
        $hasil=$this->db->query("SELECT provinsi.id as id_provinsi, provinsi.nama_provinsi, negara.id as id_negara, negara.nama_negara FROM provinsi
        INNER JOIN negara ON provinsi.id_negara = negara.id
        WHERE provinsi.id_negara = $id ORDER BY provinsi.nama_provinsi ASC");
        return $hasil->result();
    }

    public function add($post)
    {
        $status = $this->db->insert('provinsi', $post);
        return $status;
    }

    public function edit($post)
    {
        $this->db->where('id', $post['id']);
        $status = $this->db->update('provinsi', $post);
        return $status;
    }

    public function del($id)
    {
        
        $this->db->where('id', $id);
        $this->db->delete('provinsi');
    }

    public function select($table, $order)
    {
        $sql = "SELECT * FROM $table ORDER BY $order ASC";
        return $this->db->query($sql);
    }
}
?>
