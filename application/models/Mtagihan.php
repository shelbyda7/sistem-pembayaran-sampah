<?php

class Mtagihan extends CI_Model {

    public function anggota()
    {
        $query = "SELECT user.id as id, user.nik, user.nama, user.email, user.contact, SUM(tagihan.biaya) AS total_tagihan FROM user 
        JOIN tagihan ON user.id = tagihan.id_user
		WHERE tagihan.status = 'Belum Lunas' OR tagihan.status = 'Menunggu Validasi'
        GROUP BY user.id
        ORDER BY tagihan.id DESC";
        return $this->db->query($query);
    }


    public function pembayaran_tagihan($id)
    {
        $sql = "SELECT user.nama, tagihan.id AS id_tagihan, no_tagihan, tagihan.status, tagihan.biaya, tagihan.due_date, tagihan.bulan, pembayaran.status as status_pembayaran
        FROM user 
        LEFT JOIN tagihan ON tagihan.id_user = user.id
        LEFT JOIN pembayaran ON pembayaran.id_tagihan = tagihan.id
        WHERE user.id = $id";
        return $this->db->query($sql);
    }

    public function get($id = null)
    {
        $this->db->from('user');
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query;
    }

    public function detail_tagihan($id_user, $id)
    {
        $sql = "SELECT user.id AS id_user, tagihan.id AS id_tagihan, tagihan.no_tagihan, tagihan.bulan, tagihan.due_date, tagihan.biaya, tagihan.catatan, tagihan.status, created_date, email, nama, nik, contact, tagihan.id_kategori
        FROM tagihan 
        INNER JOIN user ON user.id = tagihan.id_user 
        WHERE user.id = $id_user AND tagihan.id = $id";
        return $this->db->query($sql);
    }

    public function daftar_tagihan($id = null)
    {
        $sql = "SELECT user.id AS id_user, user.nama, tagihan.id, tagihan.no_tagihan, tagihan.bulan, tagihan.due_date, tagihan.biaya, tagihan.status, tagihan.created_date
            FROM tagihan 
            LEFT JOIN user ON user.id = tagihan.id_user 
            WHERE tagihan.id_user = $id
            ORDER BY id DESC";

        return $this->db->query($sql);
    }
    
    public function check_nik($nik)
    {
        $sql = "SELECT user.id, user.nama, user.email, user.contact
        FROM user
        WHERE user.nik = '$nik' ORDER BY id DESC";
        return $this->db->query($sql);
    }

    public function check_kategori($id)
    {
        $sql = "SELECT biaya
        FROM kategori
        WHERE kategori.id = '$id'";
        return $this->db->query($sql);
    }

    public function show_data()
    {
        $sql = "SELECT log.id, log.log_number,client.nama AS nama_client, pic.nama AS nama_pic, client.category, pic.code, report_type, product.product, case_type.case_type
        FROM log
        LEFT JOIN client ON client.id = log.id_client
        LEFT JOIN product ON product.id = log.id_product
        LEFT JOIN case_type ON case_type.id = log.id_case_type
        LEFT JOIN pic ON pic.id = client.id_pic
        WHERE status='NEW' ORDER BY id DESC";
        return $this->db->query($sql);
    }


    public function select($table, $order)
    {
        $sql = "SELECT * FROM $table ORDER BY $order ASC";
        return $this->db->query($sql);
    }

    public function no_tagihan($table, $log)
    {
        $sql = "SELECT COUNT(*) + 1 AS no_tagihan FROM $table WHERE no_tagihan like '%$log%'";
        return $this->db->query($sql);
    }

    public function getDate()
    {
        $date = date("Y-m-d h:i");
        $sql = "SELECT * FROM log WHERE date_notif = '$date'";
        return $this->db->query($sql);
    }

    public function add($post)
    {
        $post = $this->input->post(null, TRUE);
        if (isset($post['tambah'])) {
			$bulan 	    = date('F, Y', strtotime($post['bulan']));
			$id_user	= $post['id_user'];
            $cek_tagihan = $this->db->query("SELECT * FROM tagihan WHERE bulan = '$bulan' AND id_user = $id_user"); 
			if ($cek_tagihan->num_rows() > 0) {
				echo '<script>
					  alert ("Tagihan Sudah Pernah Dibuat");
					  </script>';
			} else {
                $params = [
                    'no_tagihan'	=> $post['no_tagihan'],
                    'bulan'     	=> date('F, Y', strtotime($post['bulan'])),
                    'id_user'		=> $post['id_user'],
                    'due_date' 		=> $post['due_date'],
                    'biaya' 		=> (float) preg_replace("/[^0-9]/", "", $post['biaya']),
                    'catatan'   	=> $post['catatan'],
                    'status'        => 'Belum Lunas',
                    'id_kategori'   => $post['id_kategori']
                ];
                $this->db->insert('tagihan', $params);
            }
        }
    }

    public function edit($post)
    {
        $params = [
            'no_tagihan'	=> $post['no_tagihan'],
            'bulan'     	=> date('F, Y', strtotime($post['bulan'])),
            'id_user'		=> $post['id_user'],
            'due_date' 		=> $post['due_date'],
            'biaya' 		=> (float) preg_replace("/[^0-9]/", "", $post['biaya']),
            'catatan'   	=> $post['catatan'],
            'status'        => $post['status'],
            'id_kategori'   => $post['id_kategori']
        ];
        $this->db->where('id', $post['id']);
        $this->db->update('tagihan', $params);
    }

    public function del($id_user, $id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tagihan');
    }
}