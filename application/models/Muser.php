<?php

class Muser extends CI_Model
{
    public function login($post)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('username', $post['username']);
        $this->db->where('password', sha1($post['password']));
        $query = $this->db->get();
        return $query;
    }

    public function get($id = null)
    {
        $this->db->from('user');
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query;
    }

    public function admin()
    {
        $query = "SELECT * FROM user WHERE role in (1,2) ORDER BY id DESC";
        return $this->db->query($query);
    }

    public function anggota()
    {
        $query = "SELECT * FROM user WHERE role = 3 ORDER BY id DESC";
        return $this->db->query($query);
    }

    public function add($post)
    {
        $nik = $_POST['nik'];
        $username = $_POST['username'];
        $email = $_POST['email'];
        $ceknik = $this->db->query("SELECT nik FROM user WHERE nik='$nik'");
        $cekusername = $this->db->query("SELECT username FROM user WHERE username='$username'");
        $cekemail = $this->db->query("SELECT email FROM user WHERE email='$email'");
        if ($ceknik->num_rows() > 0) {
            echo '<script>
                  alert ("NIK telah terdaftar di dalam database");
                  </script>';
        } elseif ($cekusername->num_rows() > 0) {
            echo '<script>
                  alert ("Username telah terdaftar di dalam database");
                  </script>';
        } elseif ($cekemail->num_rows() > 0) {
            echo '<script>
                  alert ("Email telah terdaftar di dalam database");
                  </script>';
        } else {
            $params['nik'] = $post['nik'];
            $params['nama'] = $post['nama'];
            $params['tempat_lahir'] = $post['tempat_lahir'];
            $params['tgl_lahir'] = $post['tgl_lahir'];
            $params['jk'] = $post['jk'];
            $params['alamat'] = $post['alamat'];
            $params['contact'] = $post['contact'];
            $params['rt_rw'] = $post['rt_rw'];
            $params['kelurahan'] = $post['kelurahan'];
            $params['kecamatan'] = $post['kecamatan'];
            $params['agama'] = $post['agama'];
            $params['status_kawin'] = $post['status_kawin'];
            $params['pekerjaan'] = $post['pekerjaan'];
            $params['kewarganegaraan'] = $post['kewarganegaraan'];
            $params['username'] = $post['username'];
            $params['password'] = sha1($post['password']);
            $params['email'] = $post['email'];
            $params['contact'] = $post['contact'];
            $params['password_decrypt'] = $post['password'];
            $params['role'] = $post['role'];
            $params['id_kabupaten'] = $post['id_kabupaten'];
            
            $status = $this->db->insert('user', $params);
        }
    }

    public function edit($post)
    {
        $params['nik'] = $post['nik'];
        $params['nama'] = $post['nama'];
        $params['tempat_lahir'] = $post['tempat_lahir'];
        $params['tgl_lahir'] = $post['tgl_lahir'];
        $params['jk'] = $post['jk'];
        $params['alamat'] = $post['alamat'];
        $params['contact'] = $post['contact'];
        $params['rt_rw'] = $post['rt_rw'];
        $params['kelurahan'] = $post['kelurahan'];
        $params['kecamatan'] = $post['kecamatan'];
        $params['agama'] = $post['agama'];
        $params['status_kawin'] = $post['status_kawin'];
        $params['pekerjaan'] = $post['pekerjaan'];
        $params['kewarganegaraan'] = $post['kewarganegaraan'];
        $params['username'] = $post['username'];
        $params['password'] = sha1($post['password']);
        $params['email'] = $post['email'];
        $params['contact'] = $post['contact'];
        $params['password_decrypt'] = $post['password'];
        $params['role'] = $post['role'];
        $params['id_kabupaten'] = $post['id_kabupaten'];
        
        $this->db->where('id', $post['id']);
        $this->db->update('user', $params);
    }

    public function del($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('user');
    }
}
