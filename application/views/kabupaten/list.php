<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Master Data Kabupaten</h4>
           <button type="button" class="btn btn-success btn-rounded float-lg-right pb-1" data-toggle="modal" data-target="#modalTambahKabupaten"><i class="fas fa-plus"></i> Tambah Kabupaten Baru</button>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form>
        <div class="form-row">
            <div class="col-lg-12 offset-lg-0">
                <div class="form-row">
                    <div class="col">
                        <div class="table-responsive text-left border rounded table-striped">
                            <table class="table" id="dataTable">
                                <thead class="bg-primary text-white text-uppercase">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Negara</th>
                                        <th>Nama Provinsi</th>
                                        <th>Nama Kabupaten/Kota</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                        foreach ($result as $value) { ?>
                                            <tr>
                                                <td><?=$no++?>.</td>
                                                <td><?=$value->nama_negara?></td>
                                                <td><?=$value->nama_provinsi?></td>
                                                <td><?=$value->nama_kabupaten?></td>
                                                <td width="15%">
                                                    <button type="button" data-toggle="modal" data-target="#modalUpdateKabupaten<?=$value->id_kabupaten?>" class="btn btn-success btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <a href="<?= site_url('kabupaten/del/' . $value->id_kabupaten) ?>" onclick="return confirm('Apakah Anda Yakin')" class="btn btn-danger btn-xs">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>                           
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalTambahKabupaten" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Negara</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <form id="form" action="<?= site_url('kabupaten/add')?>" method="post">
        <div class="modal-body">
                <div class="form-row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Pilih Negara <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <select  class="form-control" id="id_negara" name="id_negara" required>
                                        <option selected value="">== PILIH NEGARA ==</option>
                                    <?php foreach ($negara as $row) { ?>
                                        <option value="<?php echo $row->id?>" ><?php echo $row->nama_negara?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Pilih Provinsi <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <select  class="id_provinsi form-control" name="id_provinsi" required>
                                    <option selected value="">== PILIH PROVINSI ==</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Nama Kabupaten <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input class="form-control" name="nama_kabupaten" id="nama_kabupaten" type="text" required placeholder="TANGERANG SELATAN" autofocus="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" name="tambah" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </form>
  </div>
</div>
</div>

<?php 
$no = 1;
foreach($result as $value) { 
$no++?>
    <!-- Modal Status -->
    <div class="modal fade" id="modalUpdateKabupaten<?=$value->id_kabupaten?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel<?php echo $value->id_kabupaten?>">Edit Kabupaten</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form" action="<?= site_url('kabupaten/edit')?>" method="post">
                <div class="modal-body">
                        <div class="form-row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Pilih Negara <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <select  class="form-control" id="id_negara<?php echo $no?>" name="id_negara" required>
                                            <?php foreach ($negara as $row) { ?>
                                                <option value="<?php echo $row->id?>" <?php if ($row->id == $value->id_negara) { ?> <?= "selected"?> <?php }?>><?php echo $row->nama_negara?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Pilih Provinsi <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <select  class="id_provinsi<?php echo $no?> form-control" name="id_provinsi" required>
                                            <?php foreach ($provinsi as $row) { ?>
                                                <option value="<?php echo $row->id?>" <?php if ($row->id == $value->id_provinsi) { ?> <?= "selected"?> <?php }?>><?php echo $row->nama_provinsi?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Nama Kabupaten <span class="text-danger">*</span></label>
                                        <div class="input-group">
                                            <input class="form-control" name="id_kabupaten" id="id_kabupaten" value="<?php echo $value->id_kabupaten?>" type="hidden" required placeholder="Dua Rumah" autofocus="">
                                            <input class="form-control" name="nama_kabupaten" id="nama_kabupaten" value="<?php echo $value->nama_kabupaten?>" type="text" required placeholder="Dua Rumah" autofocus="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" name="tambah" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
<?php } ?>

<script src="<?=base_url()?>assets/assets/libs/jquery/dist/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('#id_negara').change(function(){
            var id=$(this).val();
            console.log(id);
            $.ajax({
                url : "<?php echo base_url()?>provinsi/check_provinsi",
                method : "POST",
                data : {id:id},
                async : false,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].id_provinsi+' >'+data[i].nama_provinsi+'</option>';
                    }
                    $('.id_provinsi').html(html);
                     
                }
            });
        });
    });
<?php 
$no = 1;
foreach($result as $value) { 
$no++?>
    $(document).ready(function(){
        $('#id_negara<?php echo $no?>').change(function(){
            var id=$(this).val();
            console.log(id);
            $.ajax({
                url : "<?php echo base_url()?>provinsi/check_provinsi",
                method : "POST",
                data : {id:id},
                async : false,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].id_provinsi+' >'+data[i].nama_provinsi+'</option>';
                    }
                    $('.id_provinsi<?php echo $no?>').html(html);
                     
                }
            });
        });
    });
<?php }?>
</script>
