<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Master Data Kategori Tagihan</h4>
            <!-- <a href="<?= site_url('kategori/add')?>" type="button" class="btn btn-success btn-rounded float-lg-right pb-1"><i class="fas fa-plus"></i> Tambah Pembayaran</a> -->
            <button type="button" class="btn btn-success btn-rounded float-lg-right pb-1" data-toggle="modal" data-target="#modalTambahKategori"><i class="fas fa-plus"></i> Tambah Kategori</button>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form>
        <div class="form-row">
            <div class="col-lg-12 offset-lg-0">
                <div class="form-row">
                    <div class="col">
                        <div class="table-responsive text-left border rounded table-striped">
                            <table class="table" id="dataTable">
                                <thead class="bg-primary text-white text-uppercase">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Kategori</th>
                                        <th>Kategori Tagihan (Rp.)</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                        foreach ($kategori as $value) { ?>
                                            <tr>
                                                <td><?=$no++?>.</td>
                                                <td><?=$value->nama?></td>
                                                <td><?="Rp. " . number_format($value->biaya, 0, ".", ".")?></td>
                                                <td width="15%">
                                                    <button type="button" data-toggle="modal" data-target="#modalUpdateKategori<?=$value->id?>" class="btn btn-success btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <a href="<?= site_url('kategori/del/' . $value->id) ?>" onclick="return confirm('Apakah Anda Yakin')" class="btn btn-danger btn-xs">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>                           
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalTambahKategori" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="form" action="<?= site_url('kategori/add')?>" method="post">
        <div class="modal-body">
                <div class="form-row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Nama Kategori <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="nama" id="nama" type="text" required placeholder="Dua Rumah" autofocus="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Biaya Perbulan<span class="text-danger">*</span></label>
                            <input class="form-control" name="biaya" id="biaya" type="text" required>
                        </div>
                    </div>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <button type="submit" name="tambah" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </form>
  </div>
</div>
</div>

<?php foreach($kategori as $value) { ?>
    <!-- Modal Status -->
    <div class="modal fade" id="modalUpdateKategori<?=$value->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel<?php echo $value->id?>">Edit Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form" action="<?= site_url('kategori/edit')?>" method="post">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Nama Kategori <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $value->id?>" type="text" required placeholder="Dua Rumah" autofocus="">
                                        <input class="form-control" name="nama" id="nama" value="<?php echo $value->nama?>" type="text" required placeholder="Dua Rumah" autofocus="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Biaya Perbulan<span class="text-danger"> *</span></label>
                                    <input class="form-control" name="biaya" id="update_biaya<?php echo $value->id?>" value="<?="Rp. " . number_format($value->biaya, 0, ".", ".")?>" type="text" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                            <button type="submit" name="edit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
        
        

<script src="<?=base_url()?>assets/assets/libs/jquery/dist/jquery.min.js"></script>
<script>

    var dengan_rupiah = document.getElementById('biaya');
    dengan_rupiah.addEventListener('keyup', function(e)
    {
        dengan_rupiah.value = formatRupiah(this.value, 'Rp. ');
    });
    
    /* Fungsi */
    function formatRupiah(angka, prefix)
    {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split    = number_string.split(','),
            sisa     = split[0].length % 3,
            rupiah     = split[0].substr(0, sisa),
            ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    

    <?php foreach ($kategori as $value) { ?>
        var dengan_rupiah<?php echo $value->id ?> = document.getElementById('update_biaya<?php echo $value->id ?>');
        dengan_rupiah<?php echo $value->id ?>.addEventListener('keyup', function(e)
        {
            dengan_rupiah<?php echo $value->id ?>.value = formatRupiah(this.value, 'Rp. ');
        });

        function formatRupiah(angka, prefix)
        {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split    = number_string.split(','),
                sisa     = split[0].length % 3,
                rupiah     = split[0].substr(0, sisa),
                ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
                
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }
    <?php }?>
</script>