<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?=ucfirst($page)?> Case Type</h4>
            <a href="<?= site_url('casetype')?>" type="button" class="btn btn-success btn-rounded float-lg-right pb-1"><i class="fas fa-undo"></i> Back</a>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
    <form id="form" action ="<?= site_url('casetype/process')?>" method="post">
        <div class="form-row">
            <div class="col-sm-3 col-lg-3">
                <div class="form-group">
                    <input type="hidden" id="id" name="id" class="form-control" value="<?=$row->id?>">
                    <div class="form-group"><label>Product</label>
                    <div class="form-group">
                        <select  class="form-control" id="product" name="product">
                            <option>Product</option>
                            <?php
                                foreach ($product as $value) { ?>
                                    <option value="<?=$value->id?>" <?= $value->id == $row->id_product ? "selected" : null?>><?=$value->product?></option> 
                            <?php    }
                            ?>
                        </select>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-sm-3 col-lg-2">
                <div class="form-group">
                    <label>Channel</label>
                    <input type="text" id="channel" name="channel" class="form-control" value="<?=$row->channel?>">
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="form-group">
                    <label>Case Type</label>
                    <input type="text" id="case_type" name="case_type" class="form-control" value="<?=$row->case_type?>">
                </div>
            </div>
            <div class="col-sm-3 col-lg-2">
                <div class="form-group">
                    <label>Error Cause</label>
                    <input type="text" id="error_cause" name="error_cause" class="form-control" value="<?=$row->error_cause?>">
                </div>
            </div>
            <div class="col-lg-2">
                <div class="form-group"><label>Error Type</label>
                    <div class="form-group">
                        <select  class="form-control" id="error_type" name="error_type">
                            <option value="E" <?= $row->error_type == "E" ? "selected" : null?>>E</option>
                            <option value="N" <?= $row->error_type == "N" ? "selected" : null?>>N</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-sm-4 col-lg-12">
                <div class="form-group">
                    <label>Description</label>
                    <textarea rows="3" id="description" name="description" class="form-control" required=""  placeholder="Unit name here..."><?=$row->description?></textarea>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-lg-12" style="padding-top: 20px;">
                <button type="submit" name="<?=$page?>" class="btn btn-block btn-primary">Submit</button>
            </div>
        </div>
    </form>
    </div>
</div>