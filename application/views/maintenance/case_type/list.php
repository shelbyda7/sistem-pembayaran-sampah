<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Case Type</h4>
            <a href="<?= site_url('casetype/add')?>" type="button" class="btn btn-success btn-rounded float-lg-right pb-1"><i class="fas fa-plus"></i> Add Case Type</a>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form>
        <div class="form-row">
            <div class="col-lg-12 offset-lg-0">
                <div class="form-row">
                    <div class="col">
                        <div class="table-responsive text-left border rounded table-striped">
                            <table class="table" id="dataTable">
                                <thead class="bg-primary text-white text-uppercase">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Product</th>
                                        <th>Channel</th>
                                        <th>Case Type</th>
                                        <th>Error Type</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                        foreach ($result as $value) { ?>
                                            <tr>
                                                <td width="5%"><?=$no++?>.</td>
                                                <td><?=$value->product?></td>
                                                <td><?=$value->channel?></td>
                                                <td><?=$value->case_type?></td>
                                                <td><?=$value->error_type?></td>
                                                <td width="15%">
                                                    <a href="<?= site_url('casetype/edit/' . $value->id) ?>" class="btn btn-success btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <!-- <a href="<?= site_url('casetype/del/' . $value->id) ?>" onclick="return confirm('Apakah Anda Yakin')" class="btn btn-danger btn-xs">
                                                        <i class="fa fa-trash"></i>
                                                    </a> -->
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>                           
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>