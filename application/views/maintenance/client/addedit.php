<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?=ucfirst($page)?> Client</h4>
            <a href="<?= site_url('client')?>" type="button" class="btn btn-success btn-rounded float-lg-right pb-1"><i class="fas fa-undo"></i> Back</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-5">
        <div class="card">
            <div class="card-body">
                <form id="form" action ="<?= site_url('client/process')?>" method="post">
                    <div class="form-row">
                        <div class="col-sm-3 col-lg-12">
                            <div class="form-group">
                                <label>PIC</label>
                                <select  class="form-control" id="id_pic" name="id_pic">
                                    <option>-- Pilih --</option>
                                    <?php
                                        foreach ($pic as $value) { ?>
                                            <option value="<?=$value->id?>" <?= $value->id == $row->id_pic ? "selected" : null?>><?=$value->nama?></option> 
                                    <?php    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-12">
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="hidden" id="id" name="id" class="form-control" value="<?=$row->id?>">
                                <input type="text" id="nama" name="nama" class="form-control" value="<?=$row->nama?>">
                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-12">
                            <div class="form-group">
                                <label>Group</label>
                                <input type="text" id="group" name="group" class="form-control" value="<?=$row->group?>">
                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-12">
                            <div class="form-group">
                                <label>Cif</label>
                                <input type="text" id="cif" name="cif" class="form-control" value="<?=$row->cif?>">
                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-12">
                            <div class="form-group">
                                <label>Bsns Unt</label>
                                <input type="text" id="bsns_unt" name="bsns_unt" class="form-control" value="<?=$row->bsns_unt?>">
                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-12">
                            <div class="form-group">
                                <label>Category</label>
                                <select name="category" id="category" class="form-control" >
                                    <option>Category</option>
                                    <option value="gold" <?= $row->category == "gold" ? "selected" : null?>>Gold</option>
                                    <option value="platinum" <?= $row->category == "platinum" ? "Selected" : null?>>Platinum</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-12">
                            <div class="form-group">
                                <label>Comp Id</label>
                                <input type="text" id="comp_id" name="comp_id" class="form-control" value="<?=$row->comp_id?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-lg-12" style="padding-top: 20px;">
                            <button type="submit" name="<?= $page ?>" class="btn btn-block btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-4"></div>
</div>