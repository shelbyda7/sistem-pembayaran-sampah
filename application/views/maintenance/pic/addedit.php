<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?=ucfirst($page)?> Pic</h4>
            <a href="<?= site_url('pic')?>" type="button" class="btn btn-success btn-rounded float-lg-right pb-1"><i class="fas fa-undo"></i> Back</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-5">
        <div class="card">
            <div class="card-body">
                <form id="form" action ="<?= site_url('pic/process')?>" method="post">
                    <div class="form-row">
                        <div class="col-sm-3 col-lg-12">
                            <div class="form-group">
                                <label>Nip</label>
                                <input type="hidden" id="id" name="id" class="form-control" value="<?=$row->id?>">
                                <input type="text" id="nip" name="nip" class="form-control" value="<?=$row->nip?>">
                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-12">
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" id="nama" name="nama" class="form-control" value="<?=$row->nama?>">
                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-12">
                            <div class="form-group">
                                <label>Code</label>
                                <input type="text" id="code" name="code" class="form-control" value="<?=$row->code?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-lg-12" style="padding-top: 20px;">
                            <button type="submit" name="<?= $page ?>" class="btn btn-block btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-4"></div>
</div>