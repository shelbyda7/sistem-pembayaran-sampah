<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Master Data Negara</h4>
           <button type="button" class="btn btn-success btn-rounded float-lg-right pb-1" data-toggle="modal" data-target="#modalTambahNegara"><i class="fas fa-plus"></i> Tambah Negara Baru</button>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form>
        <div class="form-row">
            <div class="col-lg-12 offset-lg-0">
                <div class="form-row">
                    <div class="col">
                        <div class="table-responsive text-left border rounded table-striped">
                            <table class="table" id="dataTable">
                                <thead class="bg-primary text-white text-uppercase">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Negara</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                        foreach ($result as $value) { ?>
                                            <tr>
                                                <td><?=$no++?>.</td>
                                                <td><?=$value->nama_negara?></td>
                                                <td width="15%">
                                                    <button type="button" data-toggle="modal" data-target="#modalUpdateNegara<?=$value->id?>" class="btn btn-success btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <a href="<?= site_url('negara/del/' . $value->id) ?>" onclick="return confirm('Apakah Anda Yakin')" class="btn btn-danger btn-xs">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>                           
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalTambahNegara" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Negara</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="form" action="<?= site_url('negara/add')?>" method="post">
        <div class="modal-body">
                <div class="form-row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Masukkan Nama Negara <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="nama_negara" id="nama_negara" type="text" required placeholder="Negara Baru" autofocus="">
                            </div>
                        </div>
                    </div>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" name="tambah" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </form>
  </div>
</div>
</div>

<?php foreach($result as $value) { ?>
    <!-- Modal Status -->
    <div class="modal fade" id="modalUpdateNegara<?=$value->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel<?php echo $value->id?>">Edit Negara</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form" action="<?= site_url('negara/edit')?>" method="post">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Nama Negara <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $value->id?>" type="text" required placeholder="Dua Rumah" autofocus="">
                                        <input class="form-control" name="nama_negara" id="nama_negara" value="<?php echo $value->nama_negara?>" type="text" required placeholder="Dua Rumah" autofocus="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                            <button type="submit" name="edit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>