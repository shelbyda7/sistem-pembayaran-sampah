<?php if ($this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) { ?>
<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Log Pembayaran</h4>
            <button type="button" class="btn btn-success btn-rounded float-lg-right pb-1" data-toggle="modal" data-target="#modalPembayaran"><i class="fas fa-plus"></i> Tambah Pembayaran</button>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form>
        <div class="form-row">
            <div class="col-lg-12 offset-lg-0">
                <div class="form-row">
                    <div class="col">
                        <div class="table-responsive text-left border rounded table-striped">
                            <table class="table" id="dataTable">
                                <thead class="bg-primary text-white text-uppercase">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>No. Tagihan</th>
                                        <th>Biaya</th>
                                        <th>Tanggal Bayar</th>
                                        <th width="50px">Bukti</th>
                                        <th width="50px">Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                        foreach ($result as $value) { ?>
                                            <tr>
                                                <td><?=$no++?>.</td>
                                                <td><?=$value->nama?></td>
                                                <td><?=$value->no_tagihan?></td>
                                                <td><?="Rp. " . number_format($value->biaya, 0, ".", ".")?></td>
                                                <td><?=$value->tgl_bayar?></td>
                                                <td><a href="<?=site_url($value->image)?>" target="_blank"><img class="img-fluid" src="<?=site_url($value->image)?>"></a></td>
                                                <td><a href="#" <?= $value->status == 'Dalam Pengecekkan' ? "class='btn btn-warning'" : ($value->status == 'Di Terima' ? 'class="btn btn-success"' : 'class="btn btn-danger"')?>><?=$value->status?></a></td>
                                                <td width="15%">
                                                    <button type="button" data-toggle="modal" data-target="#UpdateStatusPembayaran<?= $value->id_pembayaran ?>" class="btn btn-success btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <?php if($value->status_tagihan == "Lunas") { ?>
                                                    <a href="<?= site_url('pembayaran/mail/').$value->id_pembayaran?>" onclick="return confirm('Kirim Email Status Pembayaran Sekarang?')" class="btn btn-primary btn-xs">
                                                        <i class="fa fa-envelope"></i>
                                                    </a>
                                                    <a href="<?= site_url('pembayaran/whatsapp/').$value->id_pembayaran?>" onclick="return confirm('Kirim Whatsapp Status Pembayaran Sekarang?')" class="btn btn-primary btn-xs">
                                                        <i class="fab fa-whatsapp"></i>
                                                    </a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>                           
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>

<!-- Modal Tambah Pembayaran -->
<div class="modal fade" id="modalPembayaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Tambah Pembayaran</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php echo form_open_multipart('pembayaran/proccess'); ?>
        <div class="modal-body">
                <div class="form-row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Masukkan No. Tagihan <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <input class="form-control" name="id_pembayaran" id="id_pembayaran"type="hidden" required="">
                                <input class="form-control" name="id_tagihan" id="id_tagihan"type="hidden" required="">
                                <input class="form-control" name="bulan" id="bulan"type="hidden" required="">
                                <input class="form-control" name="no_tagihan" id="no_tagihan" type="text" required placeholder="yyyymmdd-number" autofocus="">
                                <button type="button" id="Submit" name="run" class="btn btn-primary btn-check">Check</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nama</label>
                            <input class="form-control" name="nama" id="nama" type="text" readonly="" required="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Biaya</label>
                            <input class="form-control" name="biaya" id="biaya" type="text" readonly="" required="">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Status Tagihan</label>
                            <input class="form-control" name="status_tagihan" id="status_tagihan" type="text" readonly="" required="">
                            <input class="form-control" name="status_pembayaran" id="status_pembayaran" value="Dalam Pengecekkan" type="hidden" readonly="" required="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Metode Pembayaran<span class="text-danger">*</span></label>
                            <select  class="form-control" id="metode" name="metode" required>
                                <option value="Manual">Manual</option>
                                <option value="Transfer">Transfer</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="form-group">
                        <span class="id_metode text-danger" id="id_metode">Upload Bukti Pembayaran Anda</span>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Tanggal Bayar <span class="text-danger">*</span></label>
                            <input type="datetime-local" id="tgl_bayar" name="tgl_bayar" value="<?php echo date('Y-m-d\TH:i',strtotime(date('Y-m-d H:i:s'))); ?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Bukti Pembayaran <span class="text-danger">*</span></label>
                            <input class="form-control"  accept="image/*" onchange="loadFile(event)" name="image" id="image" type="file" required>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="form-group">
                        <img class="img-fluid" id="output">
                        </div>
                    </div>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" id="Submit" name="submit" value="tambah" class="btn btn-primary">Kirim Bukti</button>
            </div>
        </div>
        <?php echo form_close()?>
    </div>
    </div>
</div>
<?php $no = 1;
foreach ($result as $value) {  ?>
<!-- Modal Status Update Pembayaran -->
<div class="modal fade" id="UpdateStatusPembayaran<?= $value->id_pembayaran ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Update Data Pembayaran</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php echo form_open_multipart('pembayaran/proccess'); ?>
        <div class="modal-body">
            <div class="form-row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>No. Tagihan <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <input class="form-control" value="<?= $value->id_pembayaran ?>" name="id_pembayaran" id="id_pembayaran" type="hidden" required="">
                            <input class="form-control" value="<?= $value->id_tagihan ?>" name="id_tagihan" id="id_tagihan" type="hidden" required="">
                            <input class="form-control" value="<?= $value->bulan ?>" name="bulan" id="bulan"type="hidden" required="">
                            <input class="form-control" value="<?= $value->no_tagihan ?>" name="no_tagihan" id="no_tagihan" type="text" required placeholder="yyyymmdd-number" autofocus="" readonly="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Nama</label>
                        <input class="form-control" value="<?= $value->nama ?>" name="nama" id="nama" type="text" readonly="" required="">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Biaya</label>
                        <input class="form-control" value="<?= 'Rp. ' . number_format($value->biaya, 0, '.', '.'); ?>" name="biaya" id="biaya" type="text" readonly="" required="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Status Tagihan</label>
                        <input class="form-control" value="<?= $value->status_tagihan ?>" name="status_tagihan" id="status_tagihan" type="text" readonly="" required="">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Status Pembayaran <span class="text-danger">*</span></label>
                        <select  class="form-control" id="status_pembayaran" name="status_pembayaran" required>
                            <?php if($value->status != "Di Tolak") { ?>
                            <option value="Di Terima" <?= $value->status == 'Di Terima' ? "selected" : null?>>Di Terima</option>
                            <option value="Di Tolak" <?= $value->status == 'Di Tolak' ? "selected" : null?>>Di Tolak</option>
                            <option value="Dalam Pengecekkan" <?= $value->status == 'Dalam Pengecekkan' ? "selected" : null?>>Dalam Pengecekkan</option>
                        <?php }?>
                            <?php if($value->status == "Di Tolak") { ?>
                                <option value="Dalam Pengecekkan" <?= $value->status == 'Dalam Pengecekkan' ? "selected" : null?>>Dalam Pengecekkan</option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Tanggal Bayar <span class="text-danger">*</span></label>
                        <input type="datetime-local" id="tgl_bayar" name="tgl_bayar" value="<?php echo date('Y-m-d\TH:i',strtotime($value->tgl_bayar)); ?>" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label>Catatan <span class="text-danger">*</span></label>
                        <textarea class="form-control" name="catatan" id="catatan" rows="3" placeholder="Wajib di isi ...." required><?= $value->catatan?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Bukti Pembayaran<span class="text-danger">*</span></label>
                        <input class="form-control"  accept="image/*" onchange="loadFile<?= $value->id_pembayaran ?>(event)" name="image" id="image" type="file" readonly="" required>    
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Metode Pembayaran<span class="text-danger">*</span></label>
                        <select  class="form-control" id="metode<?= $value-> id_pembayaran ?>" name="metode" required>
                            <option value="Manual" <?= $value->metode == 'Manual' ? "selected" : null?>>Manual</option>
                            <option value="Transfer" <?= $value->metode == 'Transfer' ? "selected" : null?>>Transfer</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <span class="id_metode<?= $value-> id_pembayaran ?> text-danger" id="id_metode<?= $value-> id_pembayaran ?>">Upload Bukti Pembayaran Anda</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <img class="img-fluid" id="output<?= $value->id_pembayaran ?>" src="<?=site_url($value->image)?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="submit" value="edit" class="btn btn-primary">Update Pembayaran</button>
        </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>
<?php }?>


<script src="<?=base_url()?>assets/assets/libs/jquery/dist/jquery.min.js"></script>
<script>
$(".btn-check").on('click', function(){
    var data = $('#no_tagihan').val();
    $.ajax({
        url: "<?php echo base_url()?>pembayaran/check_no_tagihan",
        type: 'POST',
        data: {no_tagihan:data},
        dataType:'json',
        success: function(result){
            console.log(result);
            if (result.res!="N/A"){
                $("#id_tagihan").val(result.id);
                $("#bulan").val(result.bulan);
                $("#nama").val(result.nama);
                $("#biaya").val(result.biaya);
                $("#status_tagihan").val(result.status_tagihan);
            } else {
                alert("Data tidak ditemukan");
                $("#nama").val('');
                $("#biaya").val('');
                $("#status_tagihan").val('');
            }
        }
    });
});

$(document).ready(function(){
    $('#metode').change(function(){
        var value=$(this).val();
        $.ajax({
            url : "<?php echo base_url()?>pembayaran/check_metode",
            method : "POST",
            data : {value:value},
            async : false,
            dataType : 'json',
            success: function(data){
                console.log(data.bank);
                $('#id_metode').text(data.bank);
            }
        });
    });
});
<?php foreach($result as $value) { ?>
$(document).ready(function(){
    $('#metode<?= $value-> id_pembayaran ?>').change(function(){
        var value=$(this).val();
        $.ajax({
            url : "<?php echo base_url()?>pembayaran/check_metode",
            method : "POST",
            data : {value:value},
            async : false,
            dataType : 'json',
            success: function(data){
                console.log(data.bank);
                $('#id_metode<?= $value-> id_pembayaran ?>').text(data.bank);
            }
        });
    });
});
<?php } ?>

</script>
<?php foreach ($result as $value) {?>
<script>
    var loadFile<?= $value->id_pembayaran ?> = function(event) {
        var output = document.getElementById('output<?= $value->id_pembayaran?>');
        console.log(output);
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
        }
    };
</script>
<?php } ?>

<?php } elseif ($this->session->userdata('role') == 3) { ?>
    <div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Log Pembayaran Anda</h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form>
        <div class="form-row">
            <div class="col-lg-12 offset-lg-0">
                <div class="form-row">
                    <div class="col">
                        <div class="table-responsive text-left border rounded table-striped">
                            <table class="table" id="dataTable">
                                <thead class="bg-primary text-white text-uppercase">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>No. Tagihan</th>
                                        <th>Biaya</th>
                                        <th>Tanggal Bayar</th>
                                        <th width="50px">Bukti</th>
                                        <th width="50px">Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                        foreach ($result as $value) { ?>
                                            <tr>
                                                <td><?=$no++?>.</td>
                                                <td><?=$value->nama?></td>
                                                <td><?=$value->no_tagihan?></td>
                                                <td><?="Rp. " . number_format($value->biaya, 0, ".", ".")?></td>
                                                <td><?=$value->tgl_bayar?></td>
                                                <td><a href="<?=site_url($value->image)?>" target="_blank"><img class="img-fluid" src="<?=site_url($value->image)?>"></a></td>
                                                <td><a href="#" <?= $value->status == 'Dalam Pengecekkan' ? "class='btn btn-warning'" : ($value->status == 'Di Terima' ? 'class="btn btn-success"' : 'class="btn btn-danger"')?>><?=$value->status?></a></td>
                                                <?php if ($value->status == "Ditolak" || $value->status == "Dalam Pengecekkan") { ?>
                                                    <td width="15%">
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#EditPembayaranTagihan<?= $value->id_pembayaran ?>"><i class="fas fa-edit"></i> Edit</button>
                                                    </td>
                                                <?php } else { ?> 
                                                    <td width="15%">
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#EditPembayaranTagihan<?= $value->id_pembayaran ?>"><i class="fas fa-edit"></i> Edit</button>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>                           
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
<?php foreach ($result as $value) { ?>
    <div class="modal fade" id="EditPembayaranTagihan<?= $value->id_pembayaran ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Data Pembayaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open_multipart('pembayaran/proccess'); ?>
            <div class="modal-body">
                <div class="form-row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>No. Tagihan <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <input class="form-control" value="<?= $value->id_pembayaran ?>" name="id_pembayaran" id="id_pembayaran" type="hidden" required="">
                                <input class="form-control" value="<?= $value->id_tagihan ?>" name="id_tagihan" id="id_tagihan" type="hidden" required="">
                                <input class="form-control" value="<?= $value->bulan ?>" name="bulan" id="bulan"type="hidden" required="">
                                <input class="form-control" value="<?= $value->no_tagihan ?>" name="no_tagihan" id="no_tagihan" type="text" required placeholder="yyyymmdd-number" autofocus="" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nama</label>
                            <input class="form-control" value="<?= $value->nama ?>" name="nama" id="nama" type="text" readonly="" required="">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Biaya</label>
                            <input class="form-control" value="<?= 'Rp. ' . number_format($value->biaya, 0, '.', '.'); ?>" name="biaya" id="biaya" type="text" readonly="" required="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Status Tagihan</label>
                            <input class="form-control" value="<?= $value->status_tagihan ?>" name="status_tagihan" id="status_tagihan" type="text" readonly="" required="">
                            <input class="form-control" value="Dalam Pengecekkan" name="status_pembayaran" id="status_pembayaran" type="hidden" readonly="" required="">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Catatan <span class="text-danger">*</span></label>
                            <textarea class="form-control" name="catatan" id="catatan" rows="3" placeholder="Wajib di isi ...." required><?= $value->catatan?></textarea>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Tanggal Bayar <span class="text-danger">*</span></label>
                            <input type="datetime-local" id="tgl_bayar" name="tgl_bayar" value="<?php echo date('Y-m-d\TH:i',strtotime($value->tgl_bayar)); ?>" class="form-control" required readonly>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Bukti Pembayaran<span class="text-danger">*</span></label>
                            <input class="form-control"  accept="image/*" onchange="loadFile<?= $value->id_pembayaran ?>(event)" name="image" id="image" type="file" readonly="" required>    
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Metode Pembayaran<span class="text-danger">*</span></label>
                            <select  class="form-control" id="metode<?= $value-> id_pembayaran ?>" name="metode" required>
                                <option value="Manual" <?= $value->metode == 'Manual' ? "selected" : null?>>Manual</option>
                                <option value="Transfer" <?= $value->metode == 'Transfer' ? "selected" : null?>>Transfer</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <span class="id_metode<?= $value-> id_pembayaran ?> text-danger" id="id_metode<?= $value-> id_pembayaran ?>">Upload Bukti Pembayaran Anda</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <img class="img-fluid" id="output<?= $value->id_pembayaran ?>" src="<?=site_url($value->image)?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" name="submit" value="edit" class="btn btn-primary">Update Pembayaran</button>
            </div>
        <?php echo form_close()?>
        </div>
    </div>
    </div>
<?php } ?>
<script src="<?=base_url()?>assets/assets/libs/jquery/dist/jquery.min.js"></script>
<?php foreach ($result as $value) {?>
<script>
    $(document).ready(function(){
        $('#metode').change(function(){
            var value=$(this).val();
            $.ajax({
                url : "<?php echo base_url()?>pembayaran/check_metode",
                method : "POST",
                data : {value:value},
                async : false,
                dataType : 'json',
                success: function(data){
                    console.log(data.bank);
                    $('#id_metode').text(data.bank);
                }
            });
        });
    });
    var loadFile<?= $value->id_pembayaran ?> = function(event) {
        var output = document.getElementById('output<?= $value->id_pembayaran?>');
        console.log(output);
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
        }
    };
</script>
<?php } ?>
<?php } ?>