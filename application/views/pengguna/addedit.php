<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?=ucfirst($page)?> Pengguna</h4>
            <a href="<?= site_url('pengguna')?>" type="button" class="btn btn-success btn-rounded float-lg-right pb-1"><i class="fas fa-undo"></i> Back</a>
        </div>
    </div>
</div>
<form id="form" action ="<?= site_url('pengguna/process')?>" method="post">
<div class="card">
    <div class="card-body">
        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"> Kelengkapan Data Pengguna</h4>
        <hr>
        <div class="form-row">
            <div class="col-sm-3 col-lg-6">
                <div class="form-group">
                    <input type="hidden" id="id" name="id" class="form-control" value="<?=$row->id?>">
                    <div class="form-group">
                        <label>NIK (No. Induk Kependudukan) <span class="text-danger">*</span></label>
                        <input type="number" id="nik" name="nik" class="form-control" value="<?=$row->nik?>" required>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-lg-6">
                <div class="form-group">
                    <label>Nama Lengkap <span class="text-danger">*</span></label>
                    <input type="nama" id="nama" name="nama" class="form-control" value="<?=$row->nama?>" required>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-sm-3 col-lg-6">
                <div class="form-group">
                    <label>Tempat Lahir <span class="text-danger">*</span></label>
                    <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control" value="<?=$row->tempat_lahir?>" required>
                </div>
            </div>
            <div class="col-sm-3 col-lg-6">
                <div class="form-group">
                    <label>Tanggal Lahir <span class="text-danger">*</span></label>
                    <input type="datetime-local" id="tgl_lahir" name="tgl_lahir" class="form-control" value="<?=date("Y-m-d\TH:i", strtotime($row->tgl_lahir))?>" required>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-sm-3 col-lg-6">
                <div class="form-group">
                    <label>Alamat Lengkap <span class="text-danger">*</span></label>
                    <textarea id="alamat" name="alamat" rows="12" class="form-control" placeholder="Masukkan Alamat..." required><?=$row->alamat?></textarea>
                </div>
            </div>
            <div class="col-sm-3 col-lg-6">
                <div class="form-group">
                    <label>Kontak <span class="text-danger">*</span></label>
                    <input type="number" id="contact" name="contact" class="form-control" value="<?=$row->contact?>" required>
                </div>
                <div class="form-group">
                    <label>Kewarganegaraan <span class="text-danger">*</span></label>
                    <div class="form-group">
                        <select  class="form-control" id="kewarganegaraan" name="kewarganegaraan" required>
                            <option> == PILIH KEWARGANEGARAAN == </option>
                            <option value="WNI" <?= $row->kewarganegaraan == 'WNI' ? "selected" : null?>>WNI</option>
                            <option value="WNA" <?= $row->kewarganegaraan == 'WNA' ? "selected" : null?>>WNA</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                <label>Negara <span class="text-danger">*</span></label>
                    <div class="form-group">
                        <select  class="id_negara form-control" id="id_negara" name="id_negara" required>
                            <option class="negara_default"> == PILIH NEGARA == </option>
                        </select>
                    </div>
                <div class="form-group">
                    <label>Provinsi <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <select  class="id_provinsi form-control" id="id_provinsi" name="id_provinsi" required>
                                <option> == PILIH PROVINSI == </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-sm-3 col-lg-3">
                <div class="form-group">
                    <label>Kabupaten <span class="text-danger">*</span></label>
                    <div class="form-group">
                        <select  class="id_kabupaten form-control" id="id_kabupaten" name="id_kabupaten" required>
                            <option> == PILIH KABUPATEN == </option>
                            <?php foreach ($kabupaten as $value) { ?>
                                <option value="<?php echo $value->id?>" <?php if ($value->id == $row->id_kabupaten) { ?> <?= "selected"?> <?php }?>><?php echo $value->nama_kabupaten?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="form-group">
                    <label>Kecamatan <span class="text-danger">*</span></label>
                    <input type="text" id="kecamatan" name="kecamatan" class="form-control" value="<?=$row->kecamatan?>" required>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="form-group">
                    <label>Kel/Desa <span class="text-danger">*</span></label>
                    <input type="text" id="kelurahan" name="kelurahan" class="form-control" value="<?=$row->kelurahan?>" required>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <label>Jenis Kelamin <span class="text-danger">*</span></label>
                <div class="form-group">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="jk" id="inlineRadio1" value="laki-laki" checked>
                        <label class="form-check-label" for="inlineRadio1">Laki-Laki</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="jk" id="inlineRadio2" value="perempuan">
                        <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-sm-3 col-lg-3">
                <div class="form-group">
                    <label>RT/RW <span class="text-danger">*</span></label>
                    <input type="text" id="rt_rw" name="rt_rw" class="form-control" value="<?=$row->rt_rw?>" required>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="form-group">
                    <label>Agama <span class="text-danger">*</span></label>
                    <input type="text" id="agama" name="agama" class="form-control" value="<?=$row->agama?>" required>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="form-group">
                    <div class="form-group">
                        <label>Status Perkawinan <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <select  class="form-control" id="status_kawin" name="status_kawin" required>
                                <option value="Kawin" <?= $row->status_kawin == "Kawin" ? "selected" : null?>>Kawin</option>
                                <option value="Belum Kawin" <?= $row->status_kawin == "Belum Kawin" ? "selected" : null?>>Belum Kawin</option>
                                <option value="Cerai" <?= $row->status_kawin == "Cerai" ? "selected" : null?>>Cerai</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="form-group">
                    <label>Pekerjaan <span class="text-danger">*</span></label>
                    <input type="text" id="pekerjaan" name="pekerjaan" class="form-control" value="<?=$row->pekerjaan?>" required>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"> Pemberian Akses Pengguna</h4>
        <hr>
        <div class="form-row">
            <div class="col-sm-3 col-lg-6">
                <div class="form-group">
                    <input type="hidden" id="id" name="id" class="form-control" value="<?=$row->id?>">
                    <div class="form-group">
                        <label>Username <span class="text-danger">*</span></label>
                        <input type="text" id="username" name="username" class="form-control" value="<?=$row->username?>" required>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-lg-6">
                <div class="form-group">
                    <label>Password <span class="text-danger">*</span></label>
                    <input type="password" id="password" name="password" value="<?=$row->password_decrypt?>" class="form-control" required>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-sm-3 col-lg-6">
                <div class="form-group">
                    <label>Email <span class="text-danger">*</span></label>
                    <input type="email" id="email" name="email" class="form-control" value="<?=$row->email?>" required>
                </div>
            </div>
                <div class="col-sm-3 col-lg-6">
                    <div class="form-group">
                        <label>Role User <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <select  class="form-control" id="role" name="role" required>
                                <option value="1" <?= $row->role == 1 ? "selected" : null?>>Administrator</option>
                                <option value="2" <?= $row->role == 2 ? "selected" : null?>>Staff</option>
                                <option value="3" <?= $row->role == 3 ? "selected" : null?>>Anggota</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-12" style="padding-top: 20px;">
                    <button type="submit" name="<?=$page?>" class="btn btn-block btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
</form>

<script src="<?=base_url()?>assets/assets/libs/jquery/dist/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('#kewarganegaraan').change(function(){
            var value=$(this).val();
            $.ajax({
                url : "<?php echo base_url()?>pengguna/check_kewarganegaraan",
                method : "POST",
                data : {value:value},
                async : false,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    if(data.length == 1) {
                        for(i=0; i<data.length; i++){
                            html +=  '<option selected>'+'== PILIH NEGARA =='+'</option>'+'<option value='+data[i].id+'>'+data[i].nama_negara+'</option>';
                        }
                        $('.id_negara').html(html);
                    } else {
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].nama_negara+'</option>';
                        }
                        $('.id_negara').html(html);
                    }
                }
            });
        });
    });

    $(document).ready(function(){
        $('#id_negara').change(function(){
            var id=$(this).val();
            $.ajax({
                url : "<?php echo base_url()?>provinsi/check_provinsi",
                method : "POST",
                data : {id:id},
                async : false,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += 
                        '<option value='+data[i].id_provinsi+' >'+data[i].nama_provinsi+'</option>';
                    }
                    $('.id_provinsi').html(html);
                     
                }
            });
        });
    });

    $(document).ready(function(){
        $('#id_provinsi').change(function(){
            var id=$(this).val();
            console.log(id);
            $.ajax({
                url : "<?php echo base_url()?>kabupaten/check_kabupaten",
                method : "POST",
                data : {id:id},
                async : false,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += 
                        '<option value='+data[i].id_kabupaten+' >'+data[i].nama_kabupaten+'</option>';
                    }
                    $('.id_kabupaten').html(html);
                     
                }
            });
        });
    });
</script>