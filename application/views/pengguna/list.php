<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"> Daftar Pengguna</h4>
            <a href="<?= site_url('pengguna/add')?>" type="button" class="btn btn-success btn-rounded float-lg-right pb-1"><i class="fas fa-plus"></i> Tambah Pengguna</a>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-3">Menu Tab</h4>
        <ul class="nav nav-tabs mb-3">
            <li class="nav-item">
                <a href="#home" data-toggle="tab" aria-expanded="false" class="nav-link active">
                    <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                    <span class="d-none d-lg-block">Daftar Staff</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="#profile" data-toggle="tab" aria-expanded="true" class="nav-link">
                    <i class="mdi mdi-account-circle d-lg-none d-block mr-1"></i>
                    <span class="d-none d-lg-block">Daftar Anggota</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="home">
                <form>
                    <div class="form-row">
                        <div class="col-lg-12 offset-lg-0">
                            <div class="form-row">
                                <div class="col">
                                    <div class="table-responsive text-left border rounded table-striped">
                                        <table class="table" id="dataTableStaff">
                                            <thead class="bg-primary text-white text-uppercase">
                                                <tr>
                                                    <th>No</th>
                                                    <th>NIK</th>
                                                    <th>Nama</th>
                                                    <th>Username</th>
                                                    <th>Role</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $no = 1;
                                                    foreach ($staff as $value) { ?>
                                                        <tr>
                                                            <td width="5%"><?=$no++?>.</td>
                                                            <td><?=$value->nik?></td>
                                                            <td><?=$value->nama?></td>
                                                            <td><?=$value->username?></td>
                                                            <td><?=$value->role == 1 ? "Administrator" : ($value->role == 2 ? "Staff" : "Anggota")?></td>
                                                            <td width="15%">
                                                                <a href="<?= site_url('pengguna/edit/' . $value->id) ?>" class="btn btn-success btn-xs">
                                                                    <i class="fa fa-edit"></i>
                                                                </a>
                                                                <a href="<?= site_url('pengguna/del/' . $value->id) ?>" onclick="return confirm('Apakah Anda Yakin')" class="btn btn-danger btn-xs">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                <?php
                                                    }
                                                ?>
                                            </tbody>                           
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form> 
            </div>
            <div class="tab-pane show" id="profile">
            <form>
                    <div class="form-row">
                        <div class="col-lg-12 offset-lg-0">
                            <div class="form-row">
                                <div class="col">
                                    <div class="table-responsive text-left border rounded table-striped">
                                        <table class="table" id="dataTableAnggota">
                                            <thead class="bg-primary text-white text-uppercase">
                                                <tr>
                                                    <th>No</th>
                                                    <th>NIK</th>
                                                    <th>Nama</th>
                                                    <th>Username</th>
                                                    <th>Role</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $no = 1;
                                                    foreach ($anggota as $value) { ?>
                                                        <tr>
                                                            <td width="5%"><?=$no++?>.</td>
                                                            <td><?=$value->nik?></td>
                                                            <td><?=$value->nama?></td>
                                                            <td><?=$value->username?></td>
                                                            <td><?=$value->role == 1 ? "Administrator" : ($value->role == 2 ? "Staff" : "Anggota")?></td>
                                                            <td width="15%">
                                                                <a href="<?= site_url('pengguna/edit/' . $value->id) ?>" class="btn btn-success btn-xs">
                                                                    <i class="fa fa-edit"></i>
                                                                </a>
                                                                <a href="<?= site_url('pengguna/del/' . $value->id) ?>" onclick="return confirm('Apakah Anda Yakin')" class="btn btn-danger btn-xs">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                                <a href="<?= site_url('pengguna/detail/' . $value->id) ?>" class="btn btn-primary btn-xs">
                                                                    <i class="fa fa-file"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                <?php
                                                    }
                                                ?>
                                            </tbody>                           
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form> 
            </div>
        </div>

    </div> <!-- end card-body-->
</div>