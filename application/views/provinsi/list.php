<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Master Data Provinsi</h4>
           <button type="button" class="btn btn-success btn-rounded float-lg-right pb-1" data-toggle="modal" data-target="#modalTambahProvinsi"><i class="fas fa-plus"></i> Tambah Provinsi Baru</button>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form>
        <div class="form-row">
            <div class="col-lg-12 offset-lg-0">
                <div class="form-row">
                    <div class="col">
                        <div class="table-responsive text-left border rounded table-striped">
                            <table class="table" id="dataTable">
                                <thead class="bg-primary text-white text-uppercase">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Negara</th>
                                        <th>Nama Provinsi</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                        foreach ($result as $value) { ?>
                                            <tr>
                                                <td><?=$no++?>.</td>
                                                <td><?=$value->nama_negara?></td>
                                                <td><?=$value->nama_provinsi?></td>
                                                <td width="15%">
                                                    <button type="button" data-toggle="modal" data-target="#modalUpdateProvinsi<?=$value->id_provinsi?>" class="btn btn-success btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <a href="<?= site_url('provinsi/del/' . $value->id_provinsi) ?>" onclick="return confirm('Apakah Anda Yakin')" class="btn btn-danger btn-xs">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>                           
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalTambahProvinsi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Provinsi Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <form id="form" action="<?= site_url('provinsi/add')?>" method="post">
        <div class="modal-body">
                <div class="form-row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Pilih Negara <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <select  class="form-control" id="id_negara" name="id_negara" required>
                                    <?php foreach ($negara as $row) { ?>
                                        <option value="<?php echo $row->id?>" ><?php echo $row->nama_negara?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Nama Provinsi <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input class="form-control" name="nama_provinsi" id="nama_provinsi" type="text" required placeholder="Dua Rumah" autofocus="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" name="tambah" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </form>
  </div>
</div>
</div>

<?php foreach($result as $value) { ?>
    <!-- Modal Status -->
    <div class="modal fade" id="modalUpdateProvinsi<?=$value->id_provinsi?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel<?php echo $value->id_provinsi?>">Edit Provinsi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form" action="<?= site_url('provinsi/edit')?>" method="post">
                <div class="modal-body">
                        <div class="form-row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Pilih Negara <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <select  class="form-control" id="id_negara" name="id_negara" required>
                                            <?php foreach ($negara as $row) { ?>
                                                <option value="<?php echo $row->id?>" <?php if ($row->id == $value->id_negara) { ?> <?= "selected"?> <?php }?>><?php echo $row->nama_negara?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Nama Provinsi <span class="text-danger">*</span></label>
                                        <div class="input-group">
                                            <input class="form-control" name="nama_provinsi" id="nama_provinsi" value="<?php echo $value->nama_provinsi?>" type="text" required placeholder="Dua Rumah" autofocus="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" name="tambah" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
<?php } ?>