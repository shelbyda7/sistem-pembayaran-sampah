<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Log Review</h4>
            <div class="d-flex align-items-center">
                <!-- <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Apps</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Ticket List</li>
                    </ol>
                </nav> -->
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form>
            <div class="form-row">
                <div class="col-sm-3 col-lg-3">
                    <div class="form-group"><label>Log Number</label>
                        <div class="form-group">
                            <select  class="form-control" autofocus>
                                <option>--Log Number--</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-6">
                    <div class="form-group"><label>Description</label><input class="form-control" type="text" required="" inputmode="full-width-latin"></div>
                </div>
                <div class="col-sm-4 col-lg-3">
                    <div class="form-group"><label>Investigation</label><input class="form-control" type="text" required="" inputmode="full-width-latin"></div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-sm-3 col-lg-4">
                    <div class="form-group">
                        <label>Report Type</label>
                        <div class="form-group">
                            <select  class="form-control" >
                                <option value="COMPLAINT">COMPLAINT</option>
                                <option value="REQUEST">REQUEST</option>
                                <option value="INQUIRY">INQUIRY</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-lg-4">
                    <div class="form-group">
                        <label>Product</label>
                        <div class="form-group">
                            <select  class="form-control" >
                                <option value="NO">NO</option>
                                <option value="YES">YES</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-lg-4">
                    <div class="form-group">
                        <label>Case Type</label>
                        <div class="form-group">
                            <select  class="form-control" >
                                <option value="NO">NO</option>
                                <op ion value="YES">YES</op>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-12" style="padding-top: 20px;">
                    <button type="submit" id="Submit" name="submit" class="btn btn-block btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form>
        <div class="form-row">
            <div class="col-lg-12 offset-lg-0">
                <div class="form-row">
                    <div class="col">
                        <div class="table-responsive text-left border rounded">
                            <table class="table" id="dataTable">
                                <thead class="text-uppercase">
                                    <tr>
                                        <th>Log Number</th>
                                        <th>Product</th>
                                        <th>Case Type</th>
                                        <th>Report Type</th>
                                        <th>Description</th>
                                        <th>Investigation</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                    </tbody>                           
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>