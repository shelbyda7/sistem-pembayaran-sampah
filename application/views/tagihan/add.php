<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"><?=ucfirst($page)?> Tagihan</h4>
            <a href="<?= site_url('tagihan') ?>" type="button" class="btn btn-success btn-rounded float-lg-right pb-1"><i class="fas fa-undo"></i> Back</a>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form id="form" action="<?= site_url('tagihan/proccess')?>" method="post">
            <div class="form-row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Masukkan NIK Anggota</label>
                        <div class="input-group">
                            <input class="form-control" name="id_user" id="id_user" value="<?=$row->id_user?>" type="hidden" required="">
                            <input class="form-control" name="nik" id="nik" type="text" required="" placeholder="NIK Anggota" autofocus="" value="<?= $row->nik?>">
                            <button type="button" id="Submit" name="run" class="btn btn-primary btn-check">Check</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                    <label>Kategori Tagihan <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <select class="form-control" id="id_kategori" name="id_kategori" required>
                                <option value="" selected>== Pilih Kategori Tagihan ==</option>
                                <?php foreach ($kategori as $value) { ?>
                                    <option value="<?php echo $value->id?>" <?php if ($row->id_kategori == $value->id) { ?> <?= "selected"?> <?php }?>><?php echo $value->nama?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>No. Tagihan</label>
                        <input type="hidden" id="id" name="id" class="form-control" value="<?= $row->id_tagihan?>">
                        <input class="form-control" name="no_tagihan" id="no_tagihan" value="<?= $row->no_tagihan == NULL ? $date . $tagihan->no_tagihan : $row->no_tagihan?>" type="text" readonly="" required="">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-sm-3 col-lg-4">
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input class="form-control" name="nama" id="nama" type="text" value="<?=$row->nama?>" readonly="" required="">
                    </div>
                </div>
                <div class="col-sm-3 col-lg-4">
                    <div class="form-group">
                        <label>Email</label>
                        <input class="form-control" name="email" id="email" type="text" value="<?=$row->email?>" readonly="" required="">
                    </div>
                </div>
                <div class="col-sm-3 col-lg-4">
                    <div class="form-group">
                        <label>Contact</label>
                        <input class="form-control" name="contact" id="contact" type="text" value="<?=$row->contact?>" required="" readonly="">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-sm-4 col-lg-6 offset-lg-0">
                    <div class="form-group">
                        <label>Bulan Tagihan</label>
                        <input class="form-control" name="bulan" id="bulan" type="date" value="<?= date('Y-m-d', strtotime($row->bulan))?>" required="" placeholder="">
                    </div>
                </div>
                <div class="col-sm-4 col-lg-6 offset-lg-0">
                    <div class="form-group">
                        <label>Jatuh Tempo</label>
                        <input class="form-control" value="<?=$row->due_date?>" name="due_date" id="due_date" type="date" value="<?=$row->due_date?>" required="" placeholder="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Biaya/Tagihan (Rp.)</label>
                        <div class="input-group">
                            <input class="form-control" value="<?="Rp. " . number_format($row->biaya, 0, ".", ".")?>" name="biaya" id="biaya" type="text" value="<?=$row->biaya?>" required="" readonly="" placeholder="Rp. 000.000">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Status <span class="text-danger">*</span></label>
                        <div class="form-group">
                            <select  class="form-control" id="status" name="status" required>
                                <option value="Belum Lunas" <?= $row->status == "Belum Lunas" ? "selected" : null?>>Belum Lunas</option>
                                <option value="Lunas" <?= $row->status == "Lunas" ? "selected" : null?>>Lunas</option>
                                <option value="Menunggu Validasi" <?= $row->status == "Menunggu Validasi" ? "selected" : null?>>Menunggu Validasi</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label>Catatan</label>
                        <textarea class="form-control" rows="3" name="catatan" id="catatan" placeholder="Masukkan Catatan" style="margin-top: 0px; margin-bottom: 0px; height: 116px;"><?=$row->catatan?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-12" style="padding-top: 20px;">
                    <button type="submit" id="Submit" name="<?=$page?>" class="btn btn-block btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<?=base_url()?>assets/assets/libs/jquery/dist/jquery.min.js"></script>
<script>
    $(".btn-check").on('click', function(){
        var data = $('#nik').val();
        $.ajax({
            url: "<?php echo base_url()?>tagihan/check_nik",
            type: 'POST',
            data: {nik:data},
            dataType:'json',
            success: function(result){
                if (result.res!="N/A"){
                    var log = $("#no_tagihan").val();
                    $("#id_user").val(result.id);
                    $("#nama").val(result.nama);
                    $("#email").val(result.email);
                    $("#contact").val(result.contact);
                } else {
                    alert("Data tidak ditemukan");
                    $("#nama").val('');
                    $("#email").val('');
                    $("#contact").val('');
                }
            }
        });
    });

    $(document).ready(function() {
        $('#id_kategori').change(function() {
            var id_kategori = $("#id_kategori").val();
            $.ajax({
                url: "<?php echo base_url()?>tagihan/check_kategori",
                type: 'POST',
                data: {id_kategori:id_kategori},
                dataType:'json',
            }).done(function(result) {
                if (result.res!="N/A"){
                    var	number_string = result.biaya.toString(),
                        sisa 	= number_string.length % 3,
                        rupiah 	= number_string.substr(0, sisa),
                        ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
                            
                    if (ribuan) {
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }
                    $("#biaya").val('Rp.'+rupiah);
                } else {
                    alert("Data tidak ditemukan");
                    $("#biaya").val('');
                }
            })
        })
    })

    function formatRupiah(angka, prefix)
    {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split    = number_string.split(','),
            sisa     = split[0].length % 3,
            rupiah     = split[0].substr(0, sisa),
            ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>