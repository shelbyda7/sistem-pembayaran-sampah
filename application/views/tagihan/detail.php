<?php if ($this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) { ?>
<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <?php
                foreach ($anggota as $value) { ?>
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"> Daftar Tagihan <?= ucfirst($value->nama)?></h4>
            <?php } ?>
            <a href="<?= site_url('tagihan/add')?>" type="button" class="btn btn-success btn-rounded float-lg-right pb-1"><i class="fas fa-plus"></i> Tambah Tagihan <?= ucfirst($value->nama)?></a>
            <a href="<?= site_url('tagihan')?>" type="button" class="btn btn-success btn-rounded float-lg-right pb-1"><i class="fas fa-undo"></i> Back</a>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form>
        <div class="form-row">
            <div class="col-lg-12 offset-lg-0">
                <div class="form-row">
                    <div class="col">
                        <div class="table-responsive text-left border rounded table-striped">
                            <table class="table" id="dataTable">
                                <thead class="bg-primary text-uppercase text-white">
                                    <tr>
                                        <th>No</th>
                                        <th>No. Tagihan</th>
                                        <th>Bulan & Tahun</th>
                                        <th>Jatuh Tempo</th>
                                        <th>Biaya</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                        $no = 1;
                                        foreach ($tagihan as $value) { ?>
                                            <tr>
                                                <td width="5%"><?=$no++?>.</td>
                                                <td><?=$value->no_tagihan?></td>
                                                <td><?=$value->bulan?></td>
                                                <td><?=$value->due_date?></td>
                                                <td><?="Rp. " . number_format($value->biaya, 0, ".", ".")?></td>
                                                <td><a href="#" <?= $value->status == 'Lunas' ? "class='btn btn-success'" : ($value->status == 'Menunggu Validasi' ? 'class="btn btn-warning"' : 'class="btn btn-danger"')?>><?=$value->status?></a></td>
                                                <td width="15%">
                                                    <a href="<?= site_url('tagihan/edit/' . $value->id_user . '/' . $value->id) ?>" class="btn btn-success btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="<?= site_url('tagihan/del/' . $value->id_user . '/' . $value->id) ?>" onclick="return confirm('Apakah Anda Yakin ?')" class="btn btn-danger btn-xs">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                    <a href="<?= site_url('tagihan/detail/' . $value->id_user . '/' . $value->id) ?>" onclick="return confirm('Kirim Himbauan Sekarang ?!')" class="btn btn-primary btn-xs">
                                                        <i class="fa fa-envelope"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>                           
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
<?php } elseif ($this->session->userdata('role') == 3) { ?>
    <div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <?php foreach ($anggota as $value) { ?>
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1"> Daftar Tagihan <?= ucfirst($value->nama)?></h4>
            <?php } ?>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form>
        <div class="form-row">
            <div class="col-lg-12 offset-lg-0">
                <div class="form-row">
                    <div class="col">
                        <div class="table-responsive text-left border rounded table-striped">
                            <table class="table" id="dataTable">
                                <thead class="bg-primary text-uppercase text-white">
                                    <tr>
                                        <th>No</th>
                                        <th>No. Tagihan</th>
                                        <th>Bulan & Tahun</th>
                                        <th>Jatuh Tempo</th>
                                        <th>Biaya</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                        $no = 1;
                                        foreach ($result as $value) { ?>
                                            <tr>
                                                <td width="5%"><?=$no++?>.</td>
                                                <td><?=$value->no_tagihan?></td>
                                                <td><?=$value->bulan?></td>
                                                <td><?=$value->due_date?></td>
                                                <td><?="Rp. " . number_format($value->biaya, 0, ".", ".")?></td>
                                                <td><a href="#" <?= $value->status == 'Lunas' ? "class='btn btn-success'" : ($value->status == 'Menunggu Validasi' ? 'class="btn btn-warning"' : 'class="btn btn-danger"')?>><?=$value->status?></a></td>
                                                <?php if ($value->status == "Belum Lunas" && $value->status_pembayaran == NULL ) { ?>
                                                    <td width="15%">
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#BayarTagihan<?= $value->id_tagihan ?>"><i class="fas fa-plus"></i> Bayar</button>
                                                    </td>
                                                <?php } elseif ($value->status == "Menunggu Validasi" && $value->status_pembayaran == "Dalam Pengecekkan") { ?>
                                                    <td width="15%">
                                                        Mohon Menunggu Validasi Staff
                                                    </a>
                                                <?php } elseif ($value->status == "Lunas" && $value->status_pembayaran == "Di Terima") { ?>
                                                    <td width="15%">
                                                    <a href="#" class="btn btn-success btn-xs">
                                                        Terima Kasih
                                                    </a>
                                                    </td>
                                                <?php } else { ?>
                                                    <td width="15%">
                                                        Pembayaran Anda Telah Di tolak, <br> <strong><a style="color: red;" href="<?= site_url('pembayaran') ?>">Klik Disini!</a></strong>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                    <?php } ?>
                                </tbody>                           
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>

<?php $no = 1;
foreach ($result as $value) {  ?>
<!-- Modal Bayar Tagihan -->
<div class="modal fade" id="BayarTagihan<?= $value->id_tagihan ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Pembayaran Tagihan <?= $value->nama . ' ' . $value->no_tagihan ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php echo form_open_multipart('pembayaran/proccess'); ?>
        <div class="modal-body">
            <div class="form-row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>No. Tagihan <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <input class="form-control" value="<?= $value->id_tagihan ?>" name="id_tagihan" id="id_tagihan" type="hidden" required="">
                            <input class="form-control" value="<?= $value->no_tagihan ?>" name="no_tagihan" id="no_tagihan" type="text" required placeholder="yyyymmdd-number" autofocus="" readonly="">
                            <input class="form-control" value="<?= $value->bulan ?>" name="bulan" id="bulan" type="hidden" required="">
                            <input class="form-control" name="id_pembayaran" id="id_pembayaran" type="hidden" required="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Nama</label>
                        <input class="form-control" value="<?= $value->nama ?>" name="nama" id="nama" type="text" readonly="" required="">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Biaya</label>
                        <input class="form-control" name="biaya" id="biaya" value="<?= 'Rp. ' . number_format($value->biaya, 0, '.', '.'); ?>" type="text" readonly="" required="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Status Tagihan</label>
                        <input class="form-control" name="status_tagihan" id="status_tagihan" value="<?= $value->status ?>" type="text" readonly="" required="">
                        <input class="form-control" name="status_pembayaran" id="status_pembayaran" value="Dalam Pengecekkan" type="hidden" readonly="" required="">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Tanggal Bayar <span class="text-danger">*</span></label>
                        <input type="datetime-local" id="tgl_bayar" name="tgl_bayar" value="<?php echo date('Y-m-d\TH:i',strtotime(date('Y-m-d H:i:s'))); ?>" class="form-control" required readonly>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Metode Pembayaran<span class="text-danger">*</span></label>
                        <select  class="form-control" id="metode<?= $value->id_tagihan ?>" name="metode" required>
                            <option value="Manual">Manual</option>
                            <option value="Transfer">Transfer</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <span class="id_metode<?= $value->id_tagihan ?> text-danger" id="id_metode<?= $value->id_tagihan ?>">Upload Bukti Pembayaran Anda</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label>Bukti Pembayaran <span class="text-danger">*</span></label>
                        <input class="form-control"  accept="image/*" onchange="loadFile<?= $value->id_tagihan ?>(event)" name="image" id="image" type="file" required>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-12">
                    <div class="form-group">
                    <img class="img-fluid" id="output<?= $value->id_tagihan ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="submit" value="tambah" class="btn btn-primary">Kirim</button>
        </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>
<?php }?>
<script src="<?=base_url()?>assets/assets/libs/jquery/dist/jquery.min.js"></script>
<script>
<?php foreach ($result as $value) {?>
    $(document).ready(function(){
        $('#metode<?= $value->id_tagihan ?>').change(function(){
            var value=$(this).val();
            console.log(value);
            $.ajax({
                url : "<?php echo base_url()?>pembayaran/check_metode",
                method : "POST",
                data : {value:value},
                async : false,
                dataType : 'json',
                success: function(data){
                    console.log(data.bank);
                    $('#id_metode<?= $value->id_tagihan ?>').text(data.bank);
                }
            });
        });
    });
    var loadFile<?= $value->id_tagihan ?> = function(event) {
        var output = document.getElementById('output<?= $value->id_tagihan?>');
        console.log(output<?= $value->id_tagihan?>);
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
        }
    };
<?php } ?>
</script>
<?php } else { ?>
    <?= site_url("auth/logout"); ?>
<?php } ?>