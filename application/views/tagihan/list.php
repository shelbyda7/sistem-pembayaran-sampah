<div class="page-breadcrumb" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Daftar Tagihan Anggota</h4>
            <a href="<?= site_url('tagihan/add')?>" type="button" class="btn btn-success btn-rounded float-lg-right pb-1"><i class="fas fa-plus"></i> Buat Tagihan Baru</a>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form>
        <div class="form-row">
            <div class="col-lg-12 offset-lg-0">
                <div class="form-row">
                    <div class="col">
                        <div class="table-responsive text-left border rounded table-striped">
                            <table class="table" id="dataTable">
                                <thead class="bg-primary text-uppercase text-white">
                                    <tr>
                                        <th>No</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Total Tagihan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                        foreach ($anggota as $value) { ?>
                                            <tr>
                                                <td><?=$no++?>.</td>
                                                <td><?=$value->nik?></td>
                                                <td><?=$value->nama?></td>
                                                <td><?=$value->email?></td>
                                                <td><?=$value->contact?></td>
                                                <td><?="Rp. " . number_format($value->total_tagihan, 0, ".", ".")?></td>
                                                <td width="15%">
                                                    <a href="<?= site_url('tagihan/detail/' . $value->id) ?>" class="btn btn-warning btn-xs">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    <a href="<?= site_url('tagihan/detail/' . $value->id) ?>" class="btn btn-primary btn-xs">
                                                        Export
                                                    </a>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>                           
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>