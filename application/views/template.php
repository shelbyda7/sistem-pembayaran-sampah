<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>assets/assets/images/favicon.png"> -->
    <title>Chysis</title>
    <!-- This page css -->
    <!-- Custom CSS -->
    <link href="<?=base_url()?>assets/dist/css/style.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/assets/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full" data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md">
                <div class="navbar-header" data-logobg="skin6">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-brand">
                        <!-- Logo icon -->
                        <a href="<?= base_url('pengguna')?>">
                            <b class="logo-icon">
                                <!-- Dark Logo icon -->
                                <!-- <img src="<?=base_url()?>assets/assets/images/logo-icon.png" alt="homepage" class="dark-logo" /> -->
                                <!-- Light Logo icon -->
                                <!-- <img src="<?=base_url()?>assets/assets/images/logo-icon.png" alt="homepage" class="light-logo" /> -->
                            </b>
                            <!--End Logo icon -->
                            <!-- Logo text -->
                            <span class="logo-text">
                                <!-- dark Logo text -->
                                <img src="<?=base_url()?>assets/assets/images/logom.png" alt="homepage" class="dark-logo" />
                                <!-- Light Logo text -->
                                <img src="<?=base_url()?>assets/assets/images/logom.png" class="light-logo" alt="homepage" />
                            </span>
                        </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                        data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                            class="ti-more"></i></a>
                </div>
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav float-left mr-auto ml-3 pl-1"></ul>
                    <ul class="navbar-nav float-right">
                        <li class="nav-item dropdown">
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <?php if ($this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) { ?>
                            <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="<?=site_url()?>tagihan"
                                    aria-expanded="false"><i class="icon-pencil"></i><span
                                        class="hide-menu">Daftar Tagihan
                                    </span></a>
                            </li>
                        <?php } elseif ($this->session->userdata('role') == 3) { ?>
                            <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="<?=site_url()?>tagihan/detail/<?= $this->session->userdata('id') ?>"
                                    aria-expanded="false"><i class="icon-pencil"></i><span
                                        class="hide-menu">Daftar Tagihan
                                    </span></a>
                            </li>
                        <?php } ?>
                            <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="<?=site_url()?>pembayaran"
                                aria-expanded="false"><i class="icon-reload"></i><span
                                    class="hide-menu">Log Pembayaran
                                </span></a>
                            </li>
                        <?php if ($this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) { ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><i class="icon-settings"></i><span
                                    class="hide-menu">Pengaturan </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <?php if ($this->session->userdata('role') == 1) { ?>
                                <li class="sidebar-item"> <a href="<?=site_url()?>pengguna" class="sidebar-link"><i class="icon-user"></i><span 
                                            class="hide-menu"> Pengguna
                                        </span></a>
                                </li>
                                <?php } elseif ($this->session->userdata('role') == 1 || $this->session->userdata('role') == 2) { ?>
                                <li class="sidebar-item"><a href="<?=site_url()?>kategori" class="sidebar-link"><span
                                            class="hide-menu"><i class="icon-equalizer"></i>
                                            Master Kategori
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?=site_url()?>negara" class="sidebar-link"><span
                                            class="hide-menu"><i class="fas fa-globe"></i>
                                            Master Negara
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?=site_url()?>provinsi" class="sidebar-link"><span
                                            class="hide-menu"><i class="fas fa-flag"></i>
                                            Master Provinsi
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?=site_url()?>kabupaten" class="sidebar-link"><span
                                            class="hide-menu"><i class="fas fa-location-arrow"></i>
                                            Master Kabupaten/Kota
                                        </span></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php }?>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="<?=site_url()?>auth/logout"
                                aria-expanded="false"><i data-feather="log-out" class="feather-icon"></i><span
                                class="hide-menu">Logout</span></a>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <?php echo $contents ?>
                    </div>
                </div>
            </div>
            <footer class="footer text-center text-muted">
                All Rights Reserved by Adminmart. Designed and Developed by <a
                    href="#">MochiZ</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?=base_url()?>assets/assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?=base_url()?>assets/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <!-- apps -->
    <script src="<?=base_url()?>assets/dist/js/app-style-switcher.js"></script>
    <script src="<?=base_url()?>assets/dist/js/feather.min.js"></script>
    <script src="<?=base_url()?>assets/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?=base_url()?>assets/dist/js/sidebarmenu.js"></script>
    <script src="<?=base_url()?>assets/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url()?>assets/dist/js/custom.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#dataTable').DataTable();
        });
    </script>
    <script>
        $(document).ready(function(){
            $('#dataTableStaff').DataTable();
        });
    </script>
    <script>
        $(document).ready(function(){
            $('#dataTableAnggota').DataTable();
        });
    </script>
    <script>
        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
            URL.revokeObjectURL(output.src) // free memory
            }
        };
    </script>
</body>
</html>