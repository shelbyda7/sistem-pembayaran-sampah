/*
 Navicat Premium Data Transfer

 Source Server         : master_mysql
 Source Server Type    : MySQL
 Source Server Version : 100417
 Source Host           : localhost:3306
 Source Schema         : db_management

 Target Server Type    : MySQL
 Target Server Version : 100417
 File Encoding         : 65001

 Date: 27/10/2021 00:15:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for case_type
-- ----------------------------
DROP TABLE IF EXISTS `case_type`;
CREATE TABLE `case_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `channel` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `case_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `error_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `error_cause` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_product`(`id_product`) USING BTREE,
  CONSTRAINT `case_type_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of case_type
-- ----------------------------
INSERT INTO `case_type` VALUES (2, 4, 'test', 'testa', 'N', 'a', 'testt', '2021-08-23 00:41:33');

-- ----------------------------
-- Table structure for client
-- ----------------------------
DROP TABLE IF EXISTS `client`;
CREATE TABLE `client`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pic` int(11) NOT NULL,
  `cif` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `group` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `bsns_unt` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `category` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `comp_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_pic`(`id_pic`) USING BTREE,
  CONSTRAINT `client_ibfk_1` FOREIGN KEY (`id_pic`) REFERENCES `pic` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of client
-- ----------------------------
INSERT INTO `client` VALUES (2, 6, '123123456', 'AIRNAV', '-', '-', 'platinum', 'COLI001');
INSERT INTO `client` VALUES (3, 9, '123456543', 'AKAR DJATI', '-', 'Region VIII', 'gold', 'ASUW001');
INSERT INTO `client` VALUES (4, 6, '987654567', 'ARCHI', 'ARCHI GROUP', '-', 'gold', 'JNCK001');
INSERT INTO `client` VALUES (5, 9, '098092226', 'ASURANSI ALLIANZ', '-', '-', 'gold', 'SIAL001');

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_activity` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `no_log` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `log_number` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_client` int(11) NULL DEFAULT NULL,
  `sender` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `subject` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `report_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_case_type` int(11) NULL DEFAULT NULL,
  `id_product` int(11) NULL DEFAULT NULL,
  `date_inc` datetime(0) NULL DEFAULT NULL,
  `date_notif` datetime(0) NULL DEFAULT NULL,
  `date_start` datetime(0) NULL DEFAULT NULL,
  `eskalasi` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `unit_eskalasi` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `remedy` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `date_resp` datetime(0) NULL DEFAULT NULL,
  `investigation` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `solution` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `date_update` datetime(0) NULL DEFAULT NULL,
  `status` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `in_mail` int(11) NULL DEFAULT NULL,
  `out_mail` int(11) NULL DEFAULT NULL,
  `date_close` datetime(0) NULL DEFAULT NULL,
  `image` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_client`(`id_client`) USING BTREE,
  INDEX `id_case_type`(`id_case_type`) USING BTREE,
  INDEX `id_product`(`id_product`) USING BTREE,
  CONSTRAINT `log_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `log_ibfk_2` FOREIGN KEY (`id_case_type`) REFERENCES `case_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `log_ibfk_3` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of log
-- ----------------------------
INSERT INTO `log` VALUES (2, '2021-08-25 23:12:01', '250821-1', 'AVI-250821-1', 2, 'shincha255@gmail.com', 'testting', 'testing', 'COMPLAINT', 2, 1, '2021-08-25 11:11:00', '2021-08-25 11:11:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'NEW', NULL, NULL, NULL, NULL);
INSERT INTO `log` VALUES (5, '2021-09-01 23:37:37', '010921-1', 'ARI-010921-1', 4, 'siegfried3695@gmail.com', 'asd', 'sad', 'REQUEST', 2, 1, '2021-09-01 11:37:00', '2021-09-01 11:37:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'NEW', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE `pembayaran`  (
  `id` int(11) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_date` date NOT NULL,
  `status` enum('Di Terima','Dalam Pengecekkan','Di Tolak') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Dalam Pengecekkan',
  `catatan` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `id_tagihan` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pembayaran
-- ----------------------------
INSERT INTO `pembayaran` VALUES (1, '2021-10-25', '/uploads/llogo.png', '2021-10-25', 'Dalam Pengecekkan', NULL, 25);

-- ----------------------------
-- Table structure for pic
-- ----------------------------
DROP TABLE IF EXISTS `pic`;
CREATE TABLE `pic`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `code` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pic
-- ----------------------------
INSERT INTO `pic` VALUES (6, '', 'ARIO PRABOWO', 'ARI');
INSERT INTO `pic` VALUES (9, '', 'NURUL', 'NUR');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp_product` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `product` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (1, '2021-10-13 23:03:20', 'MCM');
INSERT INTO `product` VALUES (2, '2021-10-13 23:03:20', 'MFT');
INSERT INTO `product` VALUES (3, '2021-10-13 23:03:20', 'MBC');
INSERT INTO `product` VALUES (4, '2021-10-13 23:03:20', 'CASH');

-- ----------------------------
-- Table structure for tagihan
-- ----------------------------
DROP TABLE IF EXISTS `tagihan`;
CREATE TABLE `tagihan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_tagihan` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `bulan` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `due_date` date NOT NULL,
  `biaya` float(20, 0) NOT NULL,
  `catatan` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `status` enum('Lunas','Menunggu Validasi','Belum Lunas') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Belum Lunas',
  `created_date` date NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `no_tagihan`(`no_tagihan`) USING BTREE,
  INDEX `idadmin`(`id_user`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tagihan
-- ----------------------------
INSERT INTO `tagihan` VALUES (1, '20211025-1', 'January, 2021', '2021-10-26', 25000, 'Bayar Cepat', 'Belum Lunas', '2021-10-25', 34);
INSERT INTO `tagihan` VALUES (2, '20211025-2', 'February, 2021', '2021-11-26', 25000, 'Bayar Cepat', 'Belum Lunas', '2021-10-25', 34);
INSERT INTO `tagihan` VALUES (3, '20211025-3', 'January, 2021', '2021-11-26', 25000, 'Bayar Cepat', 'Belum Lunas', '2021-10-25', 32);
INSERT INTO `tagihan` VALUES (4, '20211025-4', 'February, 2021', '2021-11-26', 25000, 'Bayar Cepat', 'Belum Lunas', '2021-10-25', 32);
INSERT INTO `tagihan` VALUES (25, '20211025-5', 'January, 2021', '2021-11-03', 25000, 'catat', 'Belum Lunas', '0000-00-00', 7);
INSERT INTO `tagihan` VALUES (28, '20211025-6', 'October, 2021', '2021-11-01', 250000, 'catat', 'Menunggu Validasi', '0000-00-00', 7);
INSERT INTO `tagihan` VALUES (29, '20211025-7', 'November, 2021', '2021-11-30', 250000, 'Tagihan BAru', 'Belum Lunas', '0000-00-00', 7);
INSERT INTO `tagihan` VALUES (31, '20211025-8', 'October, 2021', '2021-10-31', 45000, 'Buat Tagihan Baru Bulan oktober', 'Belum Lunas', '0000-00-00', 8);
INSERT INTO `tagihan` VALUES (32, '20211025-9', 'October, 2021', '2022-10-30', 150000, 'Buat Oktober Tahun Depan', 'Belum Lunas', '0000-00-00', 8);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL COMMENT '1:Administrator, 2:Staff, 3:Anggota',
  `nik` varchar(17) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` datetime(0) NULL DEFAULT NULL,
  `alamat` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jk` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rt_rw` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelurahan` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kecamatan` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `agama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_kawin` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pekerjaan` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kewarganegaraan` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `nik`(`nik`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (7, 'ebyshelby', '6290eef42877cecaee9884e7e2cb353be2dd9a4b', 'shelbyda7@gmail.com', 'Shelby Dwi Anugrah', 1, '3674061505940004', 'Jakarta', '1994-05-15 00:01:00', 'Pamulang', 'laki-laki', '04/017', 'Pamulang Barat', 'Pamulang', 'Islam', 'Belum Kawin', 'Swasta', 'WNI', '');
INSERT INTO `user` VALUES (8, 'delia', '6290eef42877cecaee9884e7e2cb353be2dd9a4b', 'rahmadelia@gmail.com', 'Rahma Delia', 3, '1234567890123456', 'bogor', '1999-12-24 07:00:00', 'Bogor', 'perempuan', '04/05', 'Bogor', 'Bogor', 'Islam', 'Belum Kawin', 'Ibu Rumah Tangga', 'WNI', '');
INSERT INTO `user` VALUES (24, 'kacun', '6290eef42877cecaee9884e7e2cb353be2dd9a4b', 'kacun@gmail.com', 'Kacun', 3, '1121231321', 'bogor', '1999-12-24 07:00:00', 'Bogor', 'perempuan', '04/05', 'Bogor', 'Bogor', 'Islam', 'Belum Kawin', 'Ibu Rumah Tangga', 'WNI', '');
INSERT INTO `user` VALUES (25, 'kacun3', '6290eef42877cecaee9884e7e2cb353be2dd9a4b', 'kacun3@gmail.com', 'Kacun3', 3, '456456456465', 'bogor', '1999-12-24 07:00:00', 'Bogor', 'perempuan', '04/05', 'Bogor', 'Bogor', 'Islam', 'Belum Kawin', 'Ibu Rumah Tangga', 'WNI', '');
INSERT INTO `user` VALUES (26, 'andika', '6290eef42877cecaee9884e7e2cb353be2dd9a4b', 'andika@gmail.com', 'Andika', 3, 'ekkap', 'bogor', '1999-12-24 07:00:00', 'Bogor', 'perempuan', '04/05', 'Bogor', 'Bogor', 'Islam', 'Belum Kawin', 'Ibu Rumah Tangga', 'WNI', '');
INSERT INTO `user` VALUES (27, 'siloman', '6290eef42877cecaee9884e7e2cb353be2dd9a4b', 'siloman@gmail.com', 'Siloman', 3, '246545645', 'bogor', '1999-12-24 07:00:00', 'Bogor', 'perempuan', '04/05', 'Bogor', 'Bogor', 'Islam', 'Belum Kawin', 'Ibu Rumah Tangga', 'WNI', '');
INSERT INTO `user` VALUES (28, 'lili', '6290eef42877cecaee9884e7e2cb353be2dd9a4b', 'lili@gmail.com', 'Lili', 3, '123123213123', 'bogor', '1999-12-24 07:00:00', 'Bogor', 'perempuan', '04/05', 'Bogor', 'Bogor', 'Islam', 'Belum Kawin', 'Ibu Rumah Tangga', 'WNI', '');
INSERT INTO `user` VALUES (30, 'daffa', '6290eef42877cecaee9884e7e2cb353be2dd9a4b', 'daffa@gmail.com', 'Daffa', 3, '123123123123', 'bogor', '1999-12-24 07:00:00', 'Bogor', 'perempuan', '04/05', 'Bogor', 'Bogor', 'Islam', 'Belum Kawin', 'Ibu Rumah Tangga', 'WNI', '');
INSERT INTO `user` VALUES (31, 'dilema', '6290eef42877cecaee9884e7e2cb353be2dd9a4b', 'dilema@gmail.com', 'Dilema', 3, '12221331213', 'bogor', '1999-12-24 07:00:00', 'Bogor', 'perempuan', '04/05', 'Bogor', 'Bogor', 'Islam', 'Belum Kawin', 'Ibu Rumah Tangga', 'WNI', '');
INSERT INTO `user` VALUES (32, 'syahri', '6290eef42877cecaee9884e7e2cb353be2dd9a4b', 'syahri@gmail.com', 'Syahri', 3, '122311322311232', 'bogor', '1999-12-24 07:00:00', 'Bogor', 'perempuan', '04/05', 'Bogor', 'Bogor', 'Islam', 'Belum Kawin', 'Ibu Rumah Tangga', 'WNI', '');
INSERT INTO `user` VALUES (33, 'setiaka', '6290eef42877cecaee9884e7e2cb353be2dd9a4b', 'setiaka@gmail.com', 'Setiaka', 3, '123123121231654', 'bogor', '1999-12-24 07:00:00', 'Bogor', 'perempuan', '04/05', 'Bogor', 'Bogor', 'Islam', 'Belum Kawin', 'Ibu Rumah Tangga', 'WNI', '');
INSERT INTO `user` VALUES (34, 'selia', '6290eef42877cecaee9884e7e2cb353be2dd9a4b', 'selia@gmail.com', 'Selia', 3, '1212312312312', 'bogor', '1999-12-24 07:00:00', 'Bogor', 'perempuan', '04/05', 'Bogor', 'Bogor', 'Islam', 'Belum Kawin', 'Ibu Rumah Tangga', 'WNI', '');

-- ----------------------------
-- Function structure for getlognumber
-- ----------------------------
DROP FUNCTION IF EXISTS `getlognumber`;
delimiter ;;
CREATE FUNCTION `getlognumber`()
 RETURNS int(11)
  NO SQL 
RETURN (select COUNT(*) + 1 as kolor
FROM `input` where `input`.`no_log` like "%020320%")
;
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
